package de.evilshark.rpg.police;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class RemoveChief implements Listener {
	
	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {
		
		Entity dmg = e.getDamager();
		Entity vic = e.getEntity();
		
		if(vic.getType().equals(EntityType.VILLAGER)) {
			
			if(dmg instanceof Player) {
				
				Player p = (Player) e.getDamager();
				Villager v = (Villager) e.getEntity();
				
				String name = v.getCustomName();
				
				if(name.equals("�1Polizeichef Peter")) {
					
					if(p.hasPermission("sharkrpg.admin")) {
					
						
						if(p.getInventory().getItemInMainHand().getType().equals(Material.STICK)) {
							
							v.remove();
							p.sendMessage("�4ADMINISTRATION �8>> Du hast den NPC erfolgreich entfernt!");
							
						} else {
							
							e.setCancelled(true);
							p.sendMessage("�cWenn du diesen NPC entfernen m�chtest, nehme einen Stock in die Hand!");
							
							
						}
						
						
					} else {
						
						e.setCancelled(true);
						p.setHealth(10);
						p.sendMessage("�4Du darfst diesen NPC nicht schlagen!");
						
					}
					
				}
				
			}
			
		}
		
		
		
		
	}
	

}
