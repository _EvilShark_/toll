package de.evilshark.rpg.police;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.evilshark.rpg.common.Main;

public class EmergencyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		String msg = "";
		boolean tested = false;
		
		if(cs instanceof Player) {
			
			Player p = (Player) cs;
			
			if(args.length >= 1) {
				
				if(args[0].equalsIgnoreCase("abort")) {
					
					if(Main.emergency.contains(p.getName())) {
						p.sendMessage("�1Polizei �8>> Du hast den Vorgang abgebrochen!");
						Main.emergency.remove(p.getName());
					} else {
						p.sendMessage(Main.prefix+"�cDu hast nicht den Notruf gew�hlt!");
					}

				} else {
					
					if(Main.emergency.contains(p.getName())) {
						
						for(Player online : Bukkit.getOnlinePlayers()) {			
							
							if(tested == false) {
								
								if(online.hasPermission("sharkrpg.jobs.police")) {
									
									tested = true;
									
										for(int i = 0; i < args.length; i++) {
											msg = msg + args[i] + " ";
										}
										
										getPoliceOfficer(online, p, msg);
										
										
										
									} else {
										p.sendMessage("�1Polizei �8>> Es ist derzeit kein Polizist verf�gbar!");
										Main.emergency.remove(p.getName());
									}
								
							}	

							
						}
						
					} else {
						p.sendMessage(Main.prefix+"�cDu hast nicht den Notruf gew�hlt!");
						
					}
					
					

					
					
				}
				
				
			} else {
				p.sendMessage(Main.argument);
			}
			
			
			
		} else {
			cs.sendMessage(Main.console);
		}
		
		
		
		
		
		
		return true;
	}
	
	public static void getPoliceOfficer(Player police, Player p, String msg) {
	
		

		ItemStack duty = new ItemStack(Material.WOOL);
		ItemMeta meta = duty.getItemMeta();
		
		meta.setDisplayName("ON DUTY");
		meta.addEnchant(Enchantment.ARROW_INFINITE, 10, true);
		duty.setItemMeta(meta);
		
		if(police.getInventory().contains(duty)) {
			
			police.sendMessage("�4Notruf �8>> Spielername: �5"+p.getName());
			police.sendMessage("�4Notruf �8>> X:�5"+p.getLocation().getBlockX()+" �8Y:�5"+p.getLocation().getBlockY()+" �8Z:�5"+p.getLocation().getBlockZ());
			police.sendMessage("�4Notruf �8>> "+msg);
			
			msg = "";
			
			Main.emergency.remove(p.getName());
			
		} else {
			
			p.sendMessage("�1Polizei �8>> Es ist derzeit kein Polizist verf�gbar!");
			
		}
		
		
	}

}
