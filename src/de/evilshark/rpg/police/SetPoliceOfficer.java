package de.evilshark.rpg.police;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftVillager;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;


public class SetPoliceOfficer {
	
	public static void addNpc(Player p) {
		
		Location loc = p.getLocation();
		World w = p.getWorld();
		Villager v = (Villager) w.spawnEntity(loc, EntityType.VILLAGER);
		
		((CraftVillager)v).getHandle().setProfession(5);
		
		v.setCustomNameVisible(true);
		v.setCustomName("�1Polizeichef Peter");
		v.setAdult();
		v.getLocation().setPitch(p.getLocation().getPitch());
		v.getLocation().setYaw(p.getLocation().getYaw());
		v.getLocation().setDirection(p.getLocation().getDirection());
		v.setAI(false);
		
		p.sendMessage("�4ADMINISTRATION �8>> Du hast den NPC erfolgreich gesetzt!");
		
	}

}
