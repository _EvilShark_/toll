package de.evilshark.rpg.police;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.evilshark.rpg.common.Main;

public class PoliceDuty implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void changeDuty(PlayerInteractEntityEvent e) {
		
		
		Player p = e.getPlayer();
		
		if(e.getRightClicked().getType() == EntityType.VILLAGER) {
			
			Villager v = (Villager) e.getRightClicked();
			String name = v.getCustomName();
			
			ItemStack duty = new ItemStack(Material.WOOL);
			ItemMeta meta = duty.getItemMeta();
			
			meta.setDisplayName("ON DUTY");
			meta.addEnchant(Enchantment.ARROW_INFINITE, 10, true);
			duty.setItemMeta(meta);
			
			
			if(name.equals("�1Polizeichef Peter")) {

				e.setCancelled(true);
				
				Main.loadJobs();
				String job = Main.playerdata.getString(p.getName() + ".job");
				boolean isWorking = Main.playerdata.getBoolean(p.getName() + ".isWorking");
				
				if(job.equals("police")) {
					
					if(isWorking == false) {
						
						long current = System.currentTimeMillis();
						long end = current +  20 * 60 * 1000; 
						
						p.sendMessage(Main.prefix+"Du bist nun im Polizeidienst!");
						p.sendMessage("�6Dispatch �8>> Wir senden ihnen ab jetzt Eins�tze!");
						p.sendMessage(Main.prefix+"Komm in 20 Minuten wieder um deinen �2Lohn �8zu erhalten!");
						p.sendMessage(Main.prefix+"�6/jobs remainingtime");
						
						p.getInventory().setItem(17, duty);
						Main.playerdata.set(p.getName() + ".isWorking", true);
						Main.playerdata.set(p.getName() + ".end", end);
						Main.saveJobs();
						
						isWorking = true;
										
					} else {
					
						long current = System.currentTimeMillis();
						long end = Main.playerdata.getLong(p.getName() + ".end");
						
						if(current < end) {
							
							p.sendMessage(Main.prefix+"Du bist nun nicht mehr im Polizeidienst!");
							p.sendMessage(Main.prefix+"�cDu erh�lst keine Bezahlung, da du die Arbeit vorzeitig abgebrochen hast!");
							
							p.getInventory().remove(17);
							
							isWorking = false;
							
							Main.playerdata.set(p.getName() + ".isWorking", false);
							Main.playerdata.set(p.getName() + ".end", null);
							Main.saveJobs();
							
							
						} else {
							
							Main.loadJobs();
							Main.loadBank();
							int payday = Main.playerdata.getInt(p.getName() + ".money");
							int money = Main.finance.getInt(p.getName() + ".onBank");
							
							p.sendMessage(Main.prefix+"Du bist nun nicht mehr im Polizeidienst!");
							p.sendMessage(Main.prefix+"�cDu erh�lst �2" + payday + "$ �8als bezahlung!" );
							
							isWorking = false;
							
							Main.finance.set(p.getName() + ".onBank", money + payday);
							Main.saveBank();
							
							p.getInventory().remove(17);
							
							Main.playerdata.set(p.getName() + ".isWorking", false);
							Main.playerdata.set(p.getName() + ".end", null);
							Main.saveJobs();
							
						}
						
						
					
					}
					
					
					
				} else {
					
					p.sendMessage(Main.prefix+"�cDu bist kein Polizist!");
					
				}
				
				
				
				
			
				
			}
			
			
		}
		
		
		
	
		
		
	}
	
	

}
