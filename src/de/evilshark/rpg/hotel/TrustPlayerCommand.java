package de.evilshark.rpg.hotel;

import java.util.List;

import org.bukkit.entity.Player;

import de.evilshark.rpg.common.Main;

public class TrustPlayerCommand {
	
	
	public static void addPlayer(Player p, String trust) {
		
		Main.loadStats();
		boolean inHotel = Main.playerdata.getBoolean(p.getName() + ".inHotel");
		
		if(inHotel == true) {
			
			int room = Main.playerdata.getInt(p.getName() + ".roomNumber");
			
			Main.loadHotel();
			
			List<String> allowed = Main.hotel.getStringList(room + ".allowedPlayers");
			
			if(allowed.contains(trust)) {
				
				p.sendMessage("�6Hotel �8>> �cSie haben diesen Spieler bereits hinzugef�gt!");
				
			} else {
				
				allowed.add(trust);
				Main.hotel.set(room + ".allowedPlayers", allowed);
				Main.saveHotel();
				
				p.sendMessage("�6Hotel �8>> Dieser Spieler hat nun Zugriff auf ihr Hotelzimmer!");
				
			}
			
			 
		} else {
			
			p.sendMessage(Main.prefix+"�cDu besitzt kein Hotelzimmer!");
			
		}
		 
		
	}
	
	public static void removePlayer(Player p, String untrust) {
		
		Main.loadStats();
		boolean inHotel = Main.playerdata.getBoolean(p.getName() + ".inHotel");
		
		if(inHotel == true) {
			
			int room = Main.playerdata.getInt(p.getName() + ".roomNumber");
			
			Main.loadHotel();
			
			List<String> allowed = Main.hotel.getStringList(room + ".allowedPlayers");
			
			if(allowed != null) {
				
				if(allowed.contains(untrust)) {
					
					allowed.remove(untrust);
					Main.hotel.set(room + ".allowedPlayers", allowed);
					Main.saveHotel();
					
					p.sendMessage("�6Hotel �8>> Dieser Spieler kann nun nicht mehr auf ihr Hotelzimmer zugreifen!");
					
					
				} else {
					
					p.sendMessage("�6Hotel �8>> �cDieser Spieler wurde noch nicht hizugef�gt!");
					
				}
				
			} else {
				
				p.sendMessage("�6Hotel �8>> �cSie haben noch keine Spieler hinzugef�gt!");
				
			}
			
			
		} else {
			
			p.sendMessage(Main.prefix+"�cDu besitzt kein Hotelzimmer!");
			
		}
		
	}
	
	
	

}
