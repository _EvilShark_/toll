package de.evilshark.rpg.hotel;

import org.bukkit.entity.Player;

import de.evilshark.rpg.common.Main;

public class AdministrateHotel {
	
	public static void addRoom(Player p, String room) {
		
		double X = p.getLocation().getBlockX();
		double Y = p.getLocation().getBlockY();
		double Z = p.getLocation().getBlockZ();
		
		Main.hotel.set(room + ".XR", X);
		Main.hotel.set(room + ".YR", Y);
		Main.hotel.set(room + ".ZR", Z);
		
		Main.saveHotel();
		
		p.sendMessage("�4ADMINISTRATION �8>> Du hast die Koordinaten f�r den Raum �5"+room+" �8gesetzt!");
		
	}

}
