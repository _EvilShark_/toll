package de.evilshark.rpg.hotel;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.evilshark.rpg.common.Main;

public class CheckIn implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		
		
		Player p = (Player) e.getWhoClicked();
		String invname = e.getClickedInventory().getName();
		String name = e.getCurrentItem().getItemMeta().getDisplayName();
		Material item = e.getCurrentItem().getType();
		Material door = Material.WOOD_DOOR;
		Material boat = Material.BOAT;
		
		if(invname.equals("�6CHECK IN | �2Page 1")) {
			
			e.setCancelled(true);
			
			if(!(item.equals(boat))) {
				
				if(item.equals(door)) {
					
					if(!(e.getCurrentItem().getItemMeta().getDisplayName().contains("BESETZT"))) {
						
						int number = Integer.parseInt(name.substring(2, 5));
						int money = Main.finance.getInt(p.getName() + ".onBank");
						
						if(money < 300) {
							
							p.sendMessage(Main.prefix+"�cDu hast nicht gen�gend Geld!");
							
							
						} else {
							
							p.sendMessage("�6Hotel �8>> Sie haben sich Zimmer �5"+number+" �8gekauft!");
							p.closeInventory();
							
							Main.hotel.set(number + ".isFree", false);
							Main.hotel.set(number + ".owner", p.getName());
							Main.saveHotel();	
							
							Main.playerdata.set(p.getName() + ".inHotel", true);
							Main.playerdata.set(p.getName() + ".roomNumber", number);
							Main.saveStats();
							
							Main.finance.set(p.getName() + ".onBank", money - 300);
							Main.saveBank();
						
							Main.hotelguest.remove(p.getName());
							
							
						}
						
						
					}  else {
						
						p.sendMessage("�6Hotel �8>> �cDieses Zimmer ist besetzt!");
				
						}
					
				}
				
			} else {
				
				pageTwo(p);
				
			}
			
			
			
			
		} else if(invname.equals("�6CHECK IN | �2Page 2")) {
			
			e.setCancelled(true);
			
			if(!(item.equals(boat))) {
				
				if(item.equals(door)) {
					
					if(!(e.getCurrentItem().getItemMeta().getDisplayName().contains("BESETZT"))) {
						
						int number = Integer.parseInt(name.substring(2, 5));
						int money = Main.finance.getInt(p.getName() + ".onBank");
						
						if(money < 300) {
							
							p.sendMessage(Main.prefix+"�cDu hast nicht gen�gend Geld!");
							
						} else {
							
							p.sendMessage("�6Hotel �8>> Sie haben sich Zimmer �5"+number+" �8gekauft!");
							p.closeInventory();
							
							Main.playerdata.set(p.getName() + ".inHotel", true);
							Main.playerdata.set(p.getName() + ".roomNumber", number);
							Main.saveStats();
							
							Main.hotel.set(number + ".isFree", false);
							Main.hotel.set(number + ".owner", p.getName());
							Main.saveHotel();	
							
							Main.finance.set(p.getName() + ".onBank", money - 300);
							Main.saveBank();
							
							Main.hotelguest.remove(p.getName());
							
						}
						
						
					}  else {
						
						e.setCancelled(true);
						
						p.sendMessage("�6Hotel �8>> �cDieses Zimmer ist besetzt!");
				
					}
					
				}
				
			} else {
				e.setCancelled(true);
				pageOne(p);
				
			}
			
			
			
			
			
		}
		
		
		
		
	}
	
	
	public static void pageOne(Player p) {

		
		ItemStack boat = new ItemStack(Material.BOAT);
		ItemMeta boatm = boat.getItemMeta();
		boatm.setDisplayName("�5Seite 2");
		boat.setItemMeta(boatm);
		
		ItemStack onehone = new ItemStack(Material.WOOD_DOOR);
		ItemStack onehtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack onehthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack onehfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack onehfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack onehsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack twohone = new ItemStack(Material.WOOD_DOOR);
		ItemStack twohtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack twohthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack twohfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack twohfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack twohsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack threehone = new ItemStack(Material.WOOD_DOOR);
		ItemStack threehtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack threehthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack threehfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack threehfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack threehsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack fourhone = new ItemStack(Material.WOOD_DOOR);
		ItemStack fourhtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack fourhthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack fourhfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack fourhfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack fourhsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack fivehone = new ItemStack(Material.WOOD_DOOR);
		ItemStack fivehtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack fivehthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack fivehfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack fivehfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack fivehsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack sixhone = new ItemStack(Material.WOOD_DOOR);
		ItemStack sixhtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack sixhthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack sixhfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack sixhfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack sixhsix = new ItemStack(Material.WOOD_DOOR);
		
		
		ItemMeta meta101 = onehone.getItemMeta();
		ItemMeta meta102 = onehtwo.getItemMeta();
		ItemMeta meta103 = onehthree.getItemMeta();
		ItemMeta meta104 = onehfour.getItemMeta();
		ItemMeta meta105 = onehfive.getItemMeta();
		ItemMeta meta106 = onehsix.getItemMeta();
		
		ItemMeta meta201 = twohone.getItemMeta();
		ItemMeta meta202 = twohtwo.getItemMeta();
		ItemMeta meta203 = twohthree.getItemMeta();
		ItemMeta meta204 = twohfour.getItemMeta();
		ItemMeta meta205 = twohfive.getItemMeta();
		ItemMeta meta206 = twohsix.getItemMeta();
		
		ItemMeta meta301 = threehone.getItemMeta();
		ItemMeta meta302 = threehtwo.getItemMeta();
		ItemMeta meta303 = threehthree.getItemMeta();
		ItemMeta meta304 = threehfour.getItemMeta();
		ItemMeta meta305 = threehfive.getItemMeta();
		ItemMeta meta306 = threehsix.getItemMeta();
		
		ItemMeta meta401 = fourhone.getItemMeta();
		ItemMeta meta402 = fourhtwo.getItemMeta();
		ItemMeta meta403 = fourhthree.getItemMeta();
		ItemMeta meta404 = fourhfour.getItemMeta();
		ItemMeta meta405 = fourhfive.getItemMeta();
		ItemMeta meta406 = fourhsix.getItemMeta();
		
		ItemMeta meta501 = fivehone.getItemMeta();
		ItemMeta meta502 = fivehtwo.getItemMeta();
		ItemMeta meta503 = fivehthree.getItemMeta();
		ItemMeta meta504 = fivehfour.getItemMeta();
		ItemMeta meta505 = fivehfive.getItemMeta();
		ItemMeta meta506 = fivehsix.getItemMeta();
		
		ItemMeta meta601 = sixhone.getItemMeta();
		ItemMeta meta602 = sixhtwo.getItemMeta();
		ItemMeta meta603 = sixhthree.getItemMeta();
		ItemMeta meta604 = sixhfour.getItemMeta();
		ItemMeta meta605 = sixhfive.getItemMeta();
		ItemMeta meta606 = sixhsix.getItemMeta();
		
		
		meta101.setDisplayName("�1101 | "+ (isFree(101) ? "�2FREI" : "�4BESETZT"));
		meta102.setDisplayName("�1102 | "+ (isFree(102) ? "�2FREI" : "�4BESETZT"));
		meta103.setDisplayName("�1103 | "+ (isFree(103) ? "�2FREI" : "�4BESETZT"));
		meta104.setDisplayName("�1104 | "+ (isFree(104) ? "�2FREI" : "�4BESETZT"));
		meta105.setDisplayName("�1105 | "+ (isFree(105) ? "�2FREI" : "�4BESETZT"));
		meta106.setDisplayName("�1106 | "+ (isFree(106) ? "�2FREI" : "�4BESETZT"));
		
		meta201.setDisplayName("�1201 | "+ (isFree(201) ? "�2FREI" : "�4BESETZT"));
		meta202.setDisplayName("�1202 | "+ (isFree(202) ? "�2FREI" : "�4BESETZT"));
		meta203.setDisplayName("�1203 | "+ (isFree(203) ? "�2FREI" : "�4BESETZT"));
		meta204.setDisplayName("�1204 | "+ (isFree(204) ? "�2FREI" : "�4BESETZT"));
		meta205.setDisplayName("�1205 | "+ (isFree(205) ? "�2FREI" : "�4BESETZT"));
		meta206.setDisplayName("�1206 | "+ (isFree(206) ? "�2FREI" : "�4BESETZT"));
		
		meta301.setDisplayName("�1301 | "+ (isFree(301) ? "�2FREI" : "�4BESETZT"));
		meta302.setDisplayName("�1302 | "+ (isFree(302) ? "�2FREI" : "�4BESETZT"));
		meta303.setDisplayName("�1303 | "+ (isFree(303) ? "�2FREI" : "�4BESETZT"));
		meta304.setDisplayName("�1304 | "+ (isFree(304) ? "�2FREI" : "�4BESETZT"));
		meta305.setDisplayName("�1305 | "+ (isFree(305) ? "�2FREI" : "�4BESETZT"));
		meta306.setDisplayName("�1306 | "+ (isFree(306) ? "�2FREI" : "�4BESETZT"));
		
		meta401.setDisplayName("�1401 | "+ (isFree(401) ? "�2FREI" : "�4BESETZT"));
		meta402.setDisplayName("�1402 | "+ (isFree(402) ? "�2FREI" : "�4BESETZT"));
		meta403.setDisplayName("�1403 | "+ (isFree(403) ? "�2FREI" : "�4BESETZT"));
		meta404.setDisplayName("�1404 | "+ (isFree(404) ? "�2FREI" : "�4BESETZT"));
		meta405.setDisplayName("�1405 | "+ (isFree(405) ? "�2FREI" : "�4BESETZT"));
		meta406.setDisplayName("�1406 | "+ (isFree(406) ? "�2FREI" : "�4BESETZT"));
		
		meta501.setDisplayName("�1501 | "+ (isFree(501) ? "�2FREI" : "�4BESETZT"));
		meta502.setDisplayName("�1502 | "+ (isFree(502) ? "�2FREI" : "�4BESETZT"));
		meta503.setDisplayName("�1503 | "+ (isFree(503) ? "�2FREI" : "�4BESETZT"));
		meta504.setDisplayName("�1504 | "+ (isFree(504) ? "�2FREI" : "�4BESETZT"));
		meta505.setDisplayName("�1505 | "+ (isFree(505) ? "�2FREI" : "�4BESETZT"));
		meta506.setDisplayName("�1506 | "+ (isFree(506) ? "�2FREI" : "�4BESETZT"));
		
		meta601.setDisplayName("�1601 | "+ (isFree(601) ? "�2FREI" : "�4BESETZT"));
		meta602.setDisplayName("�1602 | "+ (isFree(602) ? "�2FREI" : "�4BESETZT"));
		meta603.setDisplayName("�1603 | "+ (isFree(603) ? "�2FREI" : "�4BESETZT"));
		meta604.setDisplayName("�1604 | "+ (isFree(604) ? "�2FREI" : "�4BESETZT"));
		meta605.setDisplayName("�1605 | "+ (isFree(605) ? "�2FREI" : "�4BESETZT"));
		meta606.setDisplayName("�1606 | "+ (isFree(606) ? "�2FREI" : "�4BESETZT"));
		
		onehone.setItemMeta(meta101);
		onehtwo.setItemMeta(meta102);
		onehthree.setItemMeta(meta103);
		onehfour.setItemMeta(meta104);
		onehfive.setItemMeta(meta105);
		onehsix.setItemMeta(meta106);
		
		twohone.setItemMeta(meta201);
		twohtwo.setItemMeta(meta202);
		twohthree.setItemMeta(meta203);
		twohfour.setItemMeta(meta204);
		twohfive.setItemMeta(meta205);
		twohsix.setItemMeta(meta206);
		
		threehone.setItemMeta(meta301);
		threehtwo.setItemMeta(meta302);
		threehthree.setItemMeta(meta303);
		threehfour.setItemMeta(meta304);
		threehfive.setItemMeta(meta305);
		threehsix.setItemMeta(meta306);
		
		fourhone.setItemMeta(meta401);
		fourhtwo.setItemMeta(meta402);
		fourhthree.setItemMeta(meta403);
		fourhfour.setItemMeta(meta404);
		fourhfive.setItemMeta(meta405);
		fourhsix.setItemMeta(meta406);
		
		fivehone.setItemMeta(meta501);
		fivehtwo.setItemMeta(meta502);
		fivehthree.setItemMeta(meta503);
		fivehfour.setItemMeta(meta504);
		fivehfive.setItemMeta(meta505);
		fivehsix.setItemMeta(meta506);
		
		sixhone.setItemMeta(meta601);
		sixhtwo.setItemMeta(meta602);
		sixhthree.setItemMeta(meta603);
		sixhfour.setItemMeta(meta604);
		sixhfive.setItemMeta(meta605);
		sixhsix.setItemMeta(meta606);
		
		
		Inventory inv = Bukkit.createInventory(null, 54, "�6CHECK IN | �2Page 1");
		
		
		inv.setItem(0, onehone);
		inv.setItem(1, onehtwo);
		inv.setItem(2, onehthree);
		inv.setItem(3, onehfour);
		inv.setItem(4, onehfive);
		inv.setItem(5, onehsix);
		inv.setItem(6, null);
		inv.setItem(7, null);
		inv.setItem(8, boat);
		
		inv.setItem(9, twohone);
		inv.setItem(10, twohtwo);
		inv.setItem(11, twohthree);
		inv.setItem(12, twohfour);
		inv.setItem(13, twohfive);
		inv.setItem(14, twohsix);
		inv.setItem(15, null);
		inv.setItem(16, null);
		inv.setItem(17, null);
		
		inv.setItem(18, threehone);
		inv.setItem(19, threehtwo);
		inv.setItem(20, threehthree);
		inv.setItem(21, threehfour);
		inv.setItem(22, threehfive);
		inv.setItem(23, threehsix);
		inv.setItem(24, null);
		inv.setItem(25, null);
		inv.setItem(26, null);
		
		inv.setItem(27, fourhone);
		inv.setItem(28, fourhtwo);
		inv.setItem(29, fourhthree);
		inv.setItem(30, fourhfour);
		inv.setItem(31, fourhfive);
		inv.setItem(32, fourhsix);
		inv.setItem(33, null);
		inv.setItem(34, null);
		inv.setItem(35, null);
		
		inv.setItem(36, fivehone);
		inv.setItem(37, fivehtwo);
		inv.setItem(38, fivehthree);
		inv.setItem(39, fivehfour);
		inv.setItem(40, fivehfive);
		inv.setItem(41, fivehsix);
		inv.setItem(42, null);
		inv.setItem(43, null);
		inv.setItem(44, null);
		
		inv.setItem(45, sixhone);
		inv.setItem(46, sixhtwo);
		inv.setItem(47, sixhthree);
		inv.setItem(48, sixhfour);
		inv.setItem(49, sixhfive);
		inv.setItem(50, sixhsix);
		inv.setItem(51, null);
		inv.setItem(52, null);
		inv.setItem(53, null);
		
		
		p.openInventory(inv);
		
		
	}
	
	public static void pageTwo(Player p) {
		
		ItemStack boat = new ItemStack(Material.BOAT);
		ItemMeta boatm = boat.getItemMeta();
		boatm.setDisplayName("�5Seite 1");
		boat.setItemMeta(boatm);
		
		ItemStack sevenhone = new ItemStack(Material.WOOD_DOOR);
		ItemStack sevenhtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack sevenhthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack sevenhfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack sevenhfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack sevenhsix = new ItemStack(Material.WOOD_DOOR);
		
		
		ItemStack eighthone = new ItemStack(Material.WOOD_DOOR);
		ItemStack eighthtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack eighththree = new ItemStack(Material.WOOD_DOOR);
		ItemStack eighthfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack eighthfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack eighthsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack ninehone = new ItemStack(Material.WOOD_DOOR);
		ItemStack ninehtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack ninehthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack ninehfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack ninehfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack ninehsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack tenhone = new ItemStack(Material.WOOD_DOOR);
		ItemStack tenhtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack tenhthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack tenhfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack tenhfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack tenhsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack elevenhone = new ItemStack(Material.WOOD_DOOR);
		ItemStack elevenhtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack elevenhthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack elevenhfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack elevenhfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack elevenhsix = new ItemStack(Material.WOOD_DOOR);
		
		ItemStack twelvehone = new ItemStack(Material.WOOD_DOOR);
		ItemStack twelvehtwo = new ItemStack(Material.WOOD_DOOR);
		ItemStack twelvehthree = new ItemStack(Material.WOOD_DOOR);
		ItemStack twelvehfour = new ItemStack(Material.WOOD_DOOR);
		ItemStack twelvehfive = new ItemStack(Material.WOOD_DOOR);
		ItemStack twelvehsix = new ItemStack(Material.WOOD_DOOR);
		
		
		
		ItemMeta meta801 = eighthone.getItemMeta();
		ItemMeta meta802 = eighthtwo.getItemMeta();
		ItemMeta meta803 = eighththree.getItemMeta();
		ItemMeta meta804 = eighthfour.getItemMeta();
		ItemMeta meta805 = eighthfive.getItemMeta();
		ItemMeta meta806 = eighthsix.getItemMeta();
		
		ItemMeta meta901 = ninehone.getItemMeta();
		ItemMeta meta902 = ninehtwo.getItemMeta();
		ItemMeta meta903 = ninehthree.getItemMeta();
		ItemMeta meta904 = ninehfour.getItemMeta();
		ItemMeta meta905 = ninehfive.getItemMeta();
		ItemMeta meta906 = ninehsix.getItemMeta();
		
		ItemMeta meta1001 = tenhone.getItemMeta();
		ItemMeta meta1002 = tenhtwo.getItemMeta();
		ItemMeta meta1003 = tenhthree.getItemMeta();
		ItemMeta meta1004 = tenhfour.getItemMeta();
		ItemMeta meta1005 = tenhfive.getItemMeta();
		ItemMeta meta1006 = tenhsix.getItemMeta();
		
		ItemMeta meta1101 = elevenhone.getItemMeta();
		ItemMeta meta1102 = elevenhtwo.getItemMeta();
		ItemMeta meta1103 = elevenhthree.getItemMeta();
		ItemMeta meta1104 = elevenhfour.getItemMeta();
		ItemMeta meta1105 = elevenhfive.getItemMeta();
		ItemMeta meta1106 = elevenhsix.getItemMeta();
		
		ItemMeta meta1201 = twelvehone.getItemMeta();
		ItemMeta meta1202 = twelvehtwo.getItemMeta();
		ItemMeta meta1203 = twelvehthree.getItemMeta();
		ItemMeta meta1204 = twelvehfour.getItemMeta();
		ItemMeta meta1205 = twelvehfive.getItemMeta();
		ItemMeta meta1206 = twelvehsix.getItemMeta();
		
		ItemMeta meta701 = sevenhone.getItemMeta();
		ItemMeta meta702 = sevenhtwo.getItemMeta();
		ItemMeta meta703 = sevenhthree.getItemMeta();
		ItemMeta meta704 = sevenhfour.getItemMeta();
		ItemMeta meta705 = sevenhfive.getItemMeta();
		ItemMeta meta706 = sevenhsix.getItemMeta();
		
		meta701.setDisplayName("�1701 | "+ (isFree(701) ? "�2FREI" : "�4BESETZT"));
		meta702.setDisplayName("�1702 | "+ (isFree(702) ? "�2FREI" : "�4BESETZT"));
		meta703.setDisplayName("�1703 | "+ (isFree(703) ? "�2FREI" : "�4BESETZT"));
		meta704.setDisplayName("�1704 | "+ (isFree(704) ? "�2FREI" : "�4BESETZT"));
		meta705.setDisplayName("�1705 | "+ (isFree(705) ? "�2FREI" : "�4BESETZT"));
		meta706.setDisplayName("�1706 | "+ (isFree(706) ? "�2FREI" : "�4BESETZT"));
		
		meta801.setDisplayName("�1801 | "+ (isFree(801) ? "�2FREI" : "�4BESETZT"));
		meta802.setDisplayName("�1802 | "+ (isFree(802) ? "�2FREI" : "�4BESETZT"));
		meta803.setDisplayName("�1803 | "+ (isFree(803) ? "�2FREI" : "�4BESETZT"));
		meta804.setDisplayName("�1804 | "+ (isFree(804) ? "�2FREI" : "�4BESETZT"));
		meta805.setDisplayName("�1805 | "+ (isFree(805) ? "�2FREI" : "�4BESETZT"));
		meta806.setDisplayName("�1806 | "+ (isFree(806) ? "�2FREI" : "�4BESETZT"));

		meta901.setDisplayName("�1901 | "+ (isFree(901) ? "�2FREI" : "�4BESETZT"));
		meta902.setDisplayName("�1902 | "+ (isFree(902) ? "�2FREI" : "�4BESETZT"));
		meta903.setDisplayName("�1903 | "+ (isFree(903) ? "�2FREI" : "�4BESETZT"));
		meta904.setDisplayName("�1904 | "+ (isFree(904) ? "�2FREI" : "�4BESETZT"));
		meta905.setDisplayName("�1905 | "+ (isFree(905) ? "�2FREI" : "�4BESETZT"));
		meta906.setDisplayName("�1906 | "+ (isFree(906) ? "�2FREI" : "�4BESETZT"));
		
		meta1001.setDisplayName("�11001 | "+ (isFree(1001) ? "�2FREI" : "�4BESETZT"));
		meta1002.setDisplayName("�11002 | "+ (isFree(1002) ? "�2FREI" : "�4BESETZT"));
		meta1003.setDisplayName("�11003 | "+ (isFree(1003) ? "�2FREI" : "�4BESETZT"));
		meta1004.setDisplayName("�11004 | "+ (isFree(1004) ? "�2FREI" : "�4BESETZT"));
		meta1005.setDisplayName("�11005 | "+ (isFree(1005) ? "�2FREI" : "�4BESETZT"));
		meta1006.setDisplayName("�11006 | "+ (isFree(1006) ? "�2FREI" : "�4BESETZT"));
		
		meta1101.setDisplayName("�11101 | "+ (isFree(1101) ? "�2FREI" : "�4BESETZT"));
		meta1102.setDisplayName("�11102 | "+ (isFree(1102) ? "�2FREI" : "�4BESETZT"));
		meta1103.setDisplayName("�11103 | "+ (isFree(1103) ? "�2FREI" : "�4BESETZT"));
		meta1104.setDisplayName("�11104 | "+ (isFree(1104) ? "�2FREI" : "�4BESETZT"));
		meta1105.setDisplayName("�11105 | "+ (isFree(1105) ? "�2FREI" : "�4BESETZT"));
		meta1106.setDisplayName("�11106 | "+ (isFree(1106) ? "�2FREI" : "�4BESETZT"));
		
		meta1201.setDisplayName("�11201");
		meta1202.setDisplayName("�11202");
		meta1203.setDisplayName("�11203");
		meta1204.setDisplayName("�11204");
		meta1205.setDisplayName("�11205");
		meta1206.setDisplayName("�11206");
		
		sevenhone.setItemMeta(meta701);
		sevenhtwo.setItemMeta(meta702);
		sevenhthree.setItemMeta(meta703);
		sevenhfour.setItemMeta(meta704);
		sevenhfive.setItemMeta(meta705);
		sevenhsix.setItemMeta(meta706);
		
		eighthone.setItemMeta(meta801);
		eighthtwo.setItemMeta(meta802);
		eighththree.setItemMeta(meta803);
		eighthfour.setItemMeta(meta804);
		eighthfive.setItemMeta(meta805);
		eighthsix.setItemMeta(meta806);
		
		ninehone.setItemMeta(meta901);
		ninehtwo.setItemMeta(meta902);
		ninehthree.setItemMeta(meta903);
		ninehfour.setItemMeta(meta904);
		ninehfive.setItemMeta(meta905);
		ninehsix.setItemMeta(meta906);
		
		tenhone.setItemMeta(meta1001);
		tenhtwo.setItemMeta(meta1002);
		tenhthree.setItemMeta(meta1003);
		tenhfour.setItemMeta(meta1004);
		tenhfive.setItemMeta(meta1005);
		tenhsix.setItemMeta(meta1006);
		
		elevenhone.setItemMeta(meta1101);
		elevenhtwo.setItemMeta(meta1102);
		elevenhthree.setItemMeta(meta1103);
		elevenhfour.setItemMeta(meta1104);
		elevenhfive.setItemMeta(meta1105);
		elevenhsix.setItemMeta(meta1106);
		
		twelvehone.setItemMeta(meta1201);
		twelvehtwo.setItemMeta(meta1202);
		twelvehthree.setItemMeta(meta1203);
		twelvehfour.setItemMeta(meta1204);
		twelvehfive.setItemMeta(meta1204);
		twelvehsix.setItemMeta(meta1205);
		
		
		Inventory inv = Bukkit.createInventory(null, 54, "�6CHECK IN | �2Page 2");
		
		inv.setItem(0, sevenhone);
		inv.setItem(1, sevenhtwo);
		inv.setItem(2, sevenhthree);
		inv.setItem(3, sevenhfour);
		inv.setItem(4, sevenhfive);
		inv.setItem(5, sevenhsix);
		inv.setItem(6, null);
		inv.setItem(7, null);
		inv.setItem(8, boat);
		
		inv.setItem(9, eighthone);
		inv.setItem(10, eighthtwo);
		inv.setItem(11, eighththree);
		inv.setItem(12, eighthfour);
		inv.setItem(13, eighthfive);
		inv.setItem(14, eighthsix);
		inv.setItem(15, null);
		inv.setItem(16, null);
		inv.setItem(17, null);
		
		inv.setItem(18, ninehone);
		inv.setItem(19, ninehtwo);
		inv.setItem(20, ninehthree);
		inv.setItem(21, ninehfour);
		inv.setItem(22, ninehfive);
		inv.setItem(23, ninehsix);
		inv.setItem(24, null);
		inv.setItem(25, null);
		inv.setItem(26, null);
		
		inv.setItem(27, ninehone);
		inv.setItem(28, ninehtwo);
		inv.setItem(29, ninehthree);
		inv.setItem(30, ninehfour);
		inv.setItem(31, ninehfive);
		inv.setItem(32, ninehsix);
		inv.setItem(33, null);
		inv.setItem(34, null);
		inv.setItem(35, null);
		
		inv.setItem(36, tenhone);
		inv.setItem(37, tenhtwo);
		inv.setItem(38, tenhthree);
		inv.setItem(39, tenhfour);
		inv.setItem(40, tenhfive);
		inv.setItem(41, tenhsix);
		inv.setItem(42, null);
		inv.setItem(43, null);
		inv.setItem(44, null);
		
		inv.setItem(45, elevenhone);
		inv.setItem(46, elevenhtwo);
		inv.setItem(47, elevenhthree);
		inv.setItem(48, elevenhfour);
		inv.setItem(49, elevenhfive);
		inv.setItem(50, elevenhsix);
		inv.setItem(51, null);
		inv.setItem(52, null);
		inv.setItem(53, null);
		
		
		p.openInventory(inv);
		
		
		
		
		
	}
	
	
	public static boolean isFree(int number) {
		
		Main.loadHotel();
		return Main.hotel.getBoolean(number + ".isFree");
		
		
	}
	

}
