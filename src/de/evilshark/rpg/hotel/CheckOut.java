package de.evilshark.rpg.hotel;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import de.evilshark.rpg.common.Main;

public class CheckOut  {

	public static void checkOut(Player p)  {
		
		boolean isInHotel = Main.playerdata.getBoolean(p.getName() + ".inHotel");
		
		if(isInHotel == true) {
			
			int room = Main.playerdata.getInt(p.getName() + ".roomNumber");
			
			p.sendMessage("�6Hotel �8>> Sie haben erfolgreich ausgecheckt, vielen Dank f�r ihren Besuch!");
			
			Main.playerdata.set(p.getName() + ".inHotel", false);
			Main.playerdata.set(p.getName() + ".roomNumber", null);
			
			Main.saveStats();
			
			Main.hotel.set(room + ".owner", null);
			Main.hotel.set(room + ".isFree", true);
			
			Main.saveHotel();
			
			Location entrance = new Location(Bukkit.getServer().getWorld("world"), 571, 72, 67);
			
			Main.hotelguest.remove(p.getName());
			p.teleport(entrance);
			
		} else {
			
			p.sendMessage("�6Hotel �8>> Sie besitzen kein Hotelzimmer!");
			
		}
		
		
		
		
	}
	
	
}
