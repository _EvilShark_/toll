package de.evilshark.rpg.hotel;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.rpg.common.Main;

public class HotelCommand implements CommandExecutor {

	@SuppressWarnings("unchecked")
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(cs instanceof Player) {
			
			Player p = (Player) cs;
			
			Main.hotelguest.add(p.getName());
			
			if(args.length == 1) {
				
				if(args[0].equalsIgnoreCase("checkin")) {
					
					
					if(Main.hotelguest.contains(p.getName())) {
						
						Main.loadHotel();
						
						if(Main.playerdata.getBoolean(p.getName() + ".inHotel") == false) {
							
							CheckIn.pageOne(p);
							p.sendMessage("�2Hotel �8>> Hier sehen sie unsere Zimmer!");
							
						} else {
							
							p.sendMessage("�2Hotel �8>> �cDu hast bereits eingecheckt!");
							p.sendMessage("�2Hotel �8>> �6/hotel info");
							
						}
						
						
					} else {
						
						p.sendMessage(Main.prefix+"�cBegebe dich erst zur Repzeption!");
						
					}
					
					
				} else if(args[0].equalsIgnoreCase("checkout")) {
					
					if(Main.hotelguest.contains(p.getName())) {
						
						CheckOut.checkOut(p);
						
					} else {
						
						p.sendMessage(Main.prefix+"�cBegebe dich erst zur Repzeption!");
						
					}

				} else if(args[0].equalsIgnoreCase("info")) {
					
					Main.loadStats();
					
					boolean inHotel = Main.playerdata.getBoolean(p.getName() + ".inHotel");
					
					if(inHotel == true) {
						
						int room = Main.playerdata.getInt(p.getName() + ".roomNumber");
						
						Main.loadHotel();
						
						if(Main.hotel.getStringList(room + ".allowedPlayers") != null) {
						
						p.sendMessage("�6Hotel �8>> Sie haben sich das Hotelzimmer �5"+room+" �8gekauft!");
						p.sendMessage("�6Hotel �8>> Diese(r) Spieler haben Zugriff auf ihr Zimmer: �5"+Main.hotel.getStringList(".allowedPlayers"));
						
						} else {
							
							p.sendMessage("�6Hotel �8>> Sie haben sich das Hotelzimmer �5"+room+" �8gekauft!");
							p.sendMessage("�6Hotel �8>> Es hat kein weiterer Spieler Zugriff auf ihr Zimmer!");
							
						}
						
						
					} else {
						
						p.sendMessage(Main.prefix+"�cDu besitzt kein Hotelzimmer!");
						
					}
					
					
					
				} else {
					
					p.sendMessage(Main.argument);
					p.sendMessage("�6/hotel [checkin/checkout/info/trust/untrust]");
					
				}
				
			} else if(args.length == 2) {
				
				
				
				if(args[0].equalsIgnoreCase("trust")) {
					
					String player = args[1];
					
					TrustPlayerCommand.addPlayer(p, player.toLowerCase());
					
					
				} else if(args[0].equalsIgnoreCase("untrust")) {
					
					String player = args[1];
					
					TrustPlayerCommand.removePlayer(p, player.toLowerCase());
					
					
				}
				
				
				
			} else {
				
				p.sendMessage(Main.argument);
				p.sendMessage("�6/hotel [checkin/checkout/info/trust/untrust]");
				
			}
			
			
			
		} else {
			
			cs.sendMessage(Main.console);
			
		}
		
		
		
		
		return true;
	}
	
	

}
