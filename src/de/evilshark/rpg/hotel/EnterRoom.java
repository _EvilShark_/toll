package de.evilshark.rpg.hotel;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import de.evilshark.rpg.common.Main;

public class EnterRoom implements Listener {

	@EventHandler
	public void enterRoom(PlayerInteractEvent e) {
		
		Player p = e.getPlayer();
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			if(e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
				
				Sign s = (Sign) e.getClickedBlock().getState();
				
				String room = s.getLine(2);
				boolean isGuest = Main.playerdata.getBoolean(p.getName() + ".inHotel");
				
				Main.loadHotel();
				
				if(isGuest == true) {
					
					int number = Main.playerdata.getInt(p.getName() + ".roomNumber"); 
					int signroom = Integer.parseInt(room);

					
					if(signroom == number) {
						
						Location roomloc = new Location(Bukkit.getWorld("world"), Main.hotel.getDouble(signroom + ".XR"), Main.hotel.getDouble(signroom + ".YR"), Main.hotel.getDouble(signroom + ".ZR"));
						
						p.sendMessage("�6Hotel �8>> Sie betreten ihr Zimmer ...");
						p.teleport(roomloc);
						
					} else {
						
						int numbero = Integer.parseInt(room);
						
						Location roomo = new Location(Bukkit.getWorld("world"), Main.hotel.getDouble(numbero + ".XR"), Main.hotel.getDouble(numbero + ".YR"), Main.hotel.getDouble(numbero + ".ZR"));
						
						Main.loadHotel();
						
						List<String> allowedPlayers = Main.hotel.getStringList(numbero + ".allowedPlayers");
						
						if(allowedPlayers.contains(p.getName().toLowerCase())) {
							
							p.teleport(roomo);
							p.sendMessage("�6Hotel �8>> Sie betreten das Zimmer von �5"+Main.hotel.getString(numbero + ".owner"));
							
						} else {
							
							p.sendMessage("�6Hotel �8>> �cDu darfst dieses Zimmer nicht betreten!");
							
						}
						
					}
						
						
					} else {
						
						int numbero = Integer.parseInt(room);
						
						Location roomo = new Location(Bukkit.getWorld("world"), Main.hotel.getDouble(numbero + ".XR"), Main.hotel.getDouble(numbero + ".YR"), Main.hotel.getDouble(numbero + ".ZR"));

						Main.loadHotel();
						
						List<String> allowedPlayers = Main.hotel.getStringList(numbero + ".allowedPlayers");
						
						if(allowedPlayers.contains(p.getName().toLowerCase())) {
							
							p.teleport(roomo);
							p.sendMessage("�6Hotel �8>> Sie betreten das Zimmer von �5"+Main.hotel.getString(numbero + ".owner"));
							
						} else {
							
							p.sendMessage("�6Hotel �8>> �cDu darfst dieses Zimmer nicht betreten!");
							
						}
						
						
					}
					
					
			}
					
					
					
				}
				
				
				
			
		}
		
		
		
		

		
	}
	
	
	
	
