package de.evilshark.rpg.finance;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class WindowInteract implements Listener {
	
	@EventHandler
	private void onClick(InventoryClickEvent e) {
		
		String name = e.getClickedInventory().getName();
		Material item = e.getCurrentItem().getType();
		Player p = (Player) e.getWhoClicked();
		
		
		if(name.equals("�6Bank Verwaltung")) {
			
			e.setCancelled(true);
			
			if(item.equals(Material.GOLD_INGOT)) {	
				e.setCancelled(true);
			} else if(item.equals(Material.DIAMOND)) {
				
				BankWindow.addMoney(p);
				
			} else if(item.equals(Material.REDSTONE)) {
				
				BankWindow.removeMoney(p);
				
			}
			
			
		} else if(name.equals("�2Einzahlung")) {
			
			if(item.equals(Material.GOLD_INGOT)) {
				
				String amount = e.getCurrentItem().getItemMeta().getDisplayName();
				
				if(amount.contains("1000")) {
					
					BankWindow.addMoneyTest(p, 1000);
					
				} else if(amount.contains("500")) {
					
					BankWindow.addMoneyTest(p, 500);
					
				} else if(amount.contains("200")) {
					
					BankWindow.addMoneyTest(p, 200);
					
				} else if(amount.contains("100")) {
					
					BankWindow.addMoneyTest(p, 100);
					
				} else if(amount.contains("50")) {
					
					BankWindow.addMoneyTest(p, 50);
					
				} else if(amount.contains("20")) {
					
					BankWindow.addMoneyTest(p, 20);
					
				} else {
					
					BankWindow.addMoneyTest(p, 10);
					
				}
				
			}
			
			e.setCancelled(true);
			
			
		} else if(name.equals("�4Auszahlung")) {
			
			e.setCancelled(true);
			
			if(item.equals(Material.GOLD_INGOT)) {
				
				String amount = e.getCurrentItem().getItemMeta().getDisplayName();
				
				if(amount.contains("1000")) {
					
					BankWindow.removeMoneyTest(p, 1000);
					
				} else if(amount.contains("500")) {
					
					BankWindow.removeMoneyTest(p, 500);
					
				} else if(amount.contains("200")) {
					
					BankWindow.removeMoneyTest(p, 200);
					
				} else if(amount.contains("100")) {
					
					BankWindow.removeMoneyTest(p, 100);
					
				} else if(amount.contains("50")) {
					
					BankWindow.removeMoneyTest(p, 50);
					
				} else if(amount.contains("20")) {
					
					BankWindow.removeMoneyTest(p, 20);
					
				} else {
					
					BankWindow.removeMoneyTest(p, 10);
					
				}
				
			}
			
		}
		
		
	}
	
	
	
}
