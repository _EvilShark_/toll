package de.evilshark.rpg.finance;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.evilshark.rpg.common.Main;

public class BankWindow {
	
	
	public static void openWindow(Player p) {
		
		Main.loadBank();
		
		ItemStack balance = new ItemStack(Material.GOLD_INGOT);
		ItemStack add = new ItemStack(Material.DIAMOND);
		ItemStack remove = new ItemStack(Material.REDSTONE);
		ItemMeta balancem = balance.getItemMeta();
		ItemMeta addm = add.getItemMeta();
		ItemMeta removem = remove.getItemMeta();
		Inventory inv = Bukkit.createInventory(null, 9, "�6Bank Verwaltung");
		int money = Main.finance.getInt(p.getName() + ".onBank");
		
		balancem.setDisplayName("�6Momentaner Kontostand: �5"+money);
		balance.setItemMeta(balancem);
		
		addm.setDisplayName("�2Einzahlung");
		add.setItemMeta(addm);
		
		removem.setDisplayName("�4Auszahlung");
		remove.setItemMeta(addm);
		
		inv.setItem(4, balance);
		inv.setItem(0, remove);
		inv.setItem(8, add);
		
		p.openInventory(inv);
		
	}
	
	public static void addMoneyTest(Player p, int value) {
		
		Main.loadBank();
		
		int money = Main.finance.getInt(p.getName() + ".onBank");
		int hand = Main.finance.getInt(p.getName() + ".onHand");
		
		if(value > hand) {
			
			p.closeInventory();
			p.sendMessage("�6Bank �8>> �cSie haben nicht gen�gend Geld!");	
			
		} else {
			
			int remain = hand - value;
			int bank = money + value;
			
			Main.finance.set(p.getName() + ".onBank", bank);
			Main.finance.set(p.getName() + ".onHand", remain);
			
			Main.saveBank();
			
			p.closeInventory();
			p.sendMessage("�6Bank �8>> Sie haben das Geld erfolgreich eingezahlt!");
			
			
		}
		
	}
	
	public static void removeMoneyTest(Player p, int value) {
		
		Main.loadBank();
		
		int money = Main.finance.getInt(p.getName() + ".onBank");
		int hand = Main.finance.getInt(p.getName() + ".onHand");
		
		if(value > money) {
			
			p.closeInventory();
			p.sendMessage("�6Bank �8>> �cSie haben nicht gen�gend Geld!");	
			
		} else {
			
			int remain = money - value;
			int out = hand + value;
			
			Main.finance.set(p.getName() + ".onBank", remain);
			Main.finance.set(p.getName() + ".onHand", out);
			
			Main.saveBank();
			
			p.closeInventory();
			p.sendMessage("�6Bank �8>> Das Geld wurde erfolgreich ausgezahlt!");
			
		}
		
	}
	
	public static void addMoney(Player p) {
		
		Inventory inv = Bukkit.createInventory(null, 9, "�2Einzahlung");
		
		ItemStack ten = new ItemStack(Material.GOLD_INGOT);
		ItemStack twenty = new ItemStack(Material.GOLD_INGOT);
		ItemStack fifty = new ItemStack(Material.GOLD_INGOT);
		ItemStack oneh = new ItemStack(Material.GOLD_INGOT);
		ItemStack twoh = new ItemStack(Material.GOLD_INGOT);
		ItemStack fiveh = new ItemStack(Material.GOLD_INGOT);
		ItemStack thousand = new ItemStack(Material.GOLD_INGOT);
		
		ItemMeta m1 = ten.getItemMeta();
		ItemMeta m2 = twenty.getItemMeta();
		ItemMeta m3 = fifty.getItemMeta();
		ItemMeta m4 = oneh.getItemMeta();
		ItemMeta m5 = twoh.getItemMeta();
		ItemMeta m6 = fiveh.getItemMeta();
		ItemMeta m7 = thousand.getItemMeta();
		
		m1.setDisplayName("�610 Dollar");
		m2.setDisplayName("�620 Dollar");
		m3.setDisplayName("�650 Dollar");
		m4.setDisplayName("�6100 Dollar");
		m5.setDisplayName("�6200 Dollar");
		m6.setDisplayName("�6500 Dollar");
		m7.setDisplayName("�61000 Dollar");
		
		ten.setItemMeta(m1);
		twenty.setItemMeta(m2);
		fifty.setItemMeta(m3);
		oneh.setItemMeta(m4);
		twoh.setItemMeta(m5);
		fiveh.setItemMeta(m6);
		thousand.setItemMeta(m7);
		
		inv.setItem(0, ten);
		inv.setItem(1, twenty);
		inv.setItem(2, fifty);
		inv.setItem(3, oneh);
		inv.setItem(4, twoh);
		inv.setItem(5, fiveh);
		inv.setItem(6, thousand);
		
		p.openInventory(inv);
		
		
	}
	
	public static void removeMoney(Player p) {
		
		Inventory inv = Bukkit.createInventory(null, 9, "�4Auszahlung");
		
		ItemStack ten = new ItemStack(Material.GOLD_INGOT);
		ItemStack twenty = new ItemStack(Material.GOLD_INGOT);
		ItemStack fifty = new ItemStack(Material.GOLD_INGOT);
		ItemStack oneh = new ItemStack(Material.GOLD_INGOT);
		ItemStack twoh = new ItemStack(Material.GOLD_INGOT);
		ItemStack fiveh = new ItemStack(Material.GOLD_INGOT);
		ItemStack thousand = new ItemStack(Material.GOLD_INGOT);
		
		ItemMeta m1 = ten.getItemMeta();
		ItemMeta m2 = twenty.getItemMeta();
		ItemMeta m3 = fifty.getItemMeta();
		ItemMeta m4 = oneh.getItemMeta();
		ItemMeta m5 = twoh.getItemMeta();
		ItemMeta m6 = fiveh.getItemMeta();
		ItemMeta m7 = thousand.getItemMeta();
		
		m1.setDisplayName("�610 Dollar");
		m2.setDisplayName("�620 Dollar");
		m3.setDisplayName("�650 Dollar");
		m4.setDisplayName("�6100 Dollar");
		m5.setDisplayName("�6200 Dollar");
		m6.setDisplayName("�6500 Dollar");
		m7.setDisplayName("�61000 Dollar");
		
		ten.setItemMeta(m1);
		twenty.setItemMeta(m2);
		fifty.setItemMeta(m3);
		oneh.setItemMeta(m4);
		twoh.setItemMeta(m5);
		fiveh.setItemMeta(m6);
		thousand.setItemMeta(m7);
		
		inv.setItem(0, ten);
		inv.setItem(1, twenty);
		inv.setItem(2, fifty);
		inv.setItem(3, oneh);
		inv.setItem(4, twoh);
		inv.setItem(5, fiveh);
		inv.setItem(6, thousand);
		
		p.openInventory(inv);
		
	}

}
