package de.evilshark.rpg.finance;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.rpg.common.Main;

public class MoneyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				Main.loadBank();
				int hand = Main.finance.getInt(p.getName() + ".onHand");
				int bank = Main.finance.getInt(p.getName() + ".onBank");
				
				p.sendMessage("�6Bank �8>> Ihr Kontostand betr�gt: �5"+bank+"�5$");
				p.sendMessage(Main.prefix+"Du tr�gst momentan �5"+hand+"�5$ �8bei dir.");
				
			} else if(args.length == 3) {
			
				if(args[0].equalsIgnoreCase("add")) {
					
					if(p.hasPermission("sharkrpg.admin.finance")) {
						
						Player p2 = Bukkit.getPlayer(args[1]);
						
						if(p2.isOnline()) {
							
							try {
								int amount = Integer.parseInt(args[2]);
								p.sendMessage("�6Bank �8>> Das Geld wurde hinzugef�gt!");
								p2.sendMessage("�6Bank �8>> Deinem Konto wurde Geld hinzugef�gt!");
								addCash(p2, amount);
							} catch(Exception exe) {
								p.sendMessage("�6Bank �8>> �cFEHLER: Bitte verwende einen anderen Wert!");
							}
							
							
						} else {
							p.sendMessage(Main.offline);
						}
						
						
					} else {
						p.sendMessage(Main.permission);
					}
				} else if(args[0].equalsIgnoreCase("remove")) {
					
					if(p.hasPermission("sharkrpg.admin.finance")) {
						
						Player p2 = Bukkit.getPlayer(args[1]);
						
						if(p2.isOnline()) {
							
							try {
								int amount = Integer.parseInt(args[2]);
								p.sendMessage("�6Bank �8>> Das Geld wurde entfernt!");
								p2.sendMessage("�6Bank �8>> Es wurde Geld von deinem Konto gel�scht!");
								removeCash(p2, amount);
							} catch(Exception exe) {
								p.sendMessage("�6Bank �8>> �cFEHLER: Bitte verwende einen anderen Wert!");
							}
							
							
						} else {
							p.sendMessage(Main.offline);
						}
						
					} else {
						p.sendMessage(Main.permission);
					}
				} else if(args[0].equalsIgnoreCase("set")) {
					
					if(p.hasPermission("sharkrpg.admin.finance")) {
						
						Player p2 = Bukkit.getPlayer(args[1]);
						
						if(p2.isOnline()) {
							
							try {
								int amount = Integer.parseInt(args[2]);
								p.sendMessage("�6Bank �8>> Das Geld wurde gestetzt!");
								p2.sendMessage("�6Bank �8>> Dein Kontostand wurde ver�ndert!");
								setCash(p2, amount);
							} catch(Exception exe) {
								p.sendMessage("�6Bank �8>> �cFEHLER: Bitte verwende einen anderen Wert!");
							}
							
							
						} else {
							p.sendMessage(Main.offline);
						}
						
					} else {
						p.sendMessage(Main.permission);
					}
				} else if(args[0].equalsIgnoreCase("transfer")) {
					
					Player p2 = Bukkit.getPlayer(args[1]);
					
					if(p2.isOnline()) {
						
						try {
							int amount = Integer.parseInt(args[2]);
							testAcc(p, p2, amount);
						} catch(Exception exe) {
							p.sendMessage("�6Bank �8>> �cFEHLER: Bitte verwende einen anderen Wert!");
						}
						
						
					} else {
						p.sendMessage(Main.offline);
					}
					
					
				} else {
					p.sendMessage(Main.argument);
				}
			} else {
				p.sendMessage(Main.argument);
			}
			
		} else {
			cs.sendMessage(Main.console);
		}
		
		
		return true;
	}
	
	public static void addCash(Player p, int amount) {
		
		Main.loadBank();
		int prev = Main.finance.getInt(p.getName() + ".onBank");
	
		Main.finance.set(p.getName() + ".onBank", prev + amount);
		Main.saveBank();
		
		
	}
	
	public static void setCash(Player p, int amount) {
		
		Main.loadBank();
	
		Main.finance.set(p.getName() + ".onBank", amount);
		Main.saveBank();
		
		
	}
	
	public static void removeCash(Player p, int amount) {
		
		Main.loadBank();
		int prev = Main.finance.getInt(p.getName() + ".onBank");
	
		Main.finance.set(p.getName() + ".onBank", prev - amount);
		Main.saveBank();
		
	}
	
	public static void testAcc(Player p, Player p2, int amount) {
		
		Main.loadBank();
		int balance = Main.finance.getInt(p.getName() + ".onBank");
		int balance2 = Main.finance.getInt(p2.getName() + ".onBank");
		
		if(balance < amount) {
			p.sendMessage("�6Server �8>> �cSie haben nicht genug Geld!");
		} else {
			Main.finance.set(p.getName() + ".onBank", balance - amount);
			Main.finance.set(p2.getName() + ".onBank", balance2 + amount);
			p.sendMessage("�6Bank �8>> Sie haben dem Spieler �5"+p2.getName()+" �6"+amount+"�6$ �8�berwiesen");
			p2.sendMessage("�6Bank �8>> Der Spieler �5"+p.getName()+" �8hat ihnen �6"+amount+"�6$ �8�berwiesen.");
		}
		
		
	}
	
	
	
	
	
	

}
