package de.evilshark.rpg.travel;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.evilshark.rpg.common.Main;

public class TrainTravel implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		
		Player p = (Player) e.getPlayer();
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			if(e.getClickedBlock().getType() == Material.WALL_SIGN || e.getClickedBlock().getType() == Material.SIGN_POST) {
				Sign s = (Sign) e.getClickedBlock().getState();
				
				String line1 = s.getLine(1);
				String line2 = s.getLine(2);
				
				if(line1.equalsIgnoreCase("�4Bahnhof")) {
					
					if(line2.contains("�5Ticketpreis")) {
						
						String sprice = s.getLine(2).substring(15, 17);
						
						Main.loadBank();
						int money = Main.finance.getInt(p.getName() + ".onBank");
						int price = Integer.parseInt(sprice);
						
						Location sloc = s.getLocation();
						Location spawntodowntown = new Location(Bukkit.getServer().getWorld("world"), 153, 69, -37);
						Location spawntotown = new Location(Bukkit.getServer().getWorld("world"), 159, 69, -42);
						Location spawntorich = new Location(Bukkit.getServer().getWorld("world"), 165, 69, -38);
						
						Location downtowntospawn = new Location(Bukkit.getServer().getWorld("world"), 545, 64, 34);
						Location downtowntotown = new Location(Bukkit.getServer().getWorld("world"), 540, 64, 28);
						Location downtowntorich = new Location(Bukkit.getServer().getWorld("world"), 544, 64, 22);
						
						Location spawntodowntownstation = new Location(Bukkit.getServer().getWorld("world"), 151, 68, -38);
						Location spawntotownstation = new Location(Bukkit.getServer().getWorld("world"), 160, 68, -44);
						Location spawntorichstation = new Location(Bukkit.getServer().getWorld("world"), 167, 68, -37);
						
						Location downtowntospawnstation = new Location(Bukkit.getServer().getWorld("world"), 544, 63, 36);
						Location downtowntotownstation = new Location(Bukkit.getServer().getWorld("world"), 538, 63, 27);
						Location downtowntorichstation = new Location(Bukkit.getServer().getWorld("world"), 545, 62, 20);
						
						if(money < price) {
							p.sendMessage(Main.prefix+"�cDu hast nicht gen�gend Geld! (Bank)");
						} else {
							
							if(sloc.equals(spawntodowntown)) {
								p.sendMessage(Main.prefix+"Du hast ein Bahnticket erworben!");
								Main.finance.set(p.getName() + ".onBank", money - price);
								Main.saveBank();
								p.teleport(spawntodowntownstation);
							} else if(sloc.equals(spawntotown)) {
								p.sendMessage(Main.prefix+"Du hast ein Bahnticket erworben!");
								Main.finance.set(p.getName() + ".onBank", money - price);
								Main.saveBank();
								p.teleport(spawntotownstation);
							} else if(sloc.equals(spawntorich)) {
								p.sendMessage(Main.prefix+"Du hast ein Bahnticket erworben!");
								Main.finance.set(p.getName() + ".onBank", money - price);
								Main.saveBank();
								p.teleport(spawntorichstation);
								
							} else if(sloc.equals(downtowntorich)) {
								p.sendMessage(Main.prefix+"Du hast ein Bahnticket erworben!");
								Main.finance.set(p.getName() + ".onBank", money - price);
								Main.saveBank();
								p.teleport(downtowntorichstation);
							} else if(sloc.equals(downtowntotown)) {
								p.sendMessage(Main.prefix+"Du hast ein Bahnticket erworben!");
								Main.finance.set(p.getName() + ".onBank", money - price);
								Main.saveBank();
							 	p.teleport(downtowntotownstation);
							} else if(sloc.equals(downtowntospawn)) {
								p.sendMessage(Main.prefix+"Du hast ein Bahnticket erworben!");
								Main.finance.set(p.getName() + ".onBank", money - price);
								Main.saveBank();
								p.teleport(downtowntospawnstation);	
							}
							
							
						}
										
					} else if(line2.contains("�2Ziel")) {
						
						String objective = s.getLine(2).substring(8);
						
						Location wait = new Location(Bukkit.getServer().getWorld("world"), 149, 68, -17);
						
						Location spawnstation = new Location(Bukkit.getServer().getWorld("world"), 160, 77, -25);
						Location downtownstation = new Location(Bukkit.getServer().getWorld("world"), 557, 72, 27);
						
						if(objective.equalsIgnoreCase("Downtown")) {
							
							p.teleport(wait);
							p.sendMessage("�5Zuglautsprecher �8>> Ankunft in Downtown in 10 Sekunden");
							p.setGameMode(GameMode.ADVENTURE);
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*10, 10));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20*10, 10));
							p.playSound(p.getLocation(), Sound.ENTITY_MINECART_RIDING, 1, 1);
							
							Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
								
								@Override
								public void run() {
									
									p.sendMessage("�5Zuglautsprecher �8>> Ankunft in Downtown ...");
									p.sendMessage("�5Zuglautsprecher �8>> Beehren sie uns bald wieder!");
									p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
									p.removePotionEffect(PotionEffectType.INVISIBILITY);
									p.setGameMode(GameMode.SURVIVAL);
									p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
									p.teleport(downtownstation);										
								}
							}, 200);
							
							
						} else if(objective.equalsIgnoreCase("Spawn")) {
							
							p.teleport(wait);
							p.sendMessage("�5Zuglautsprecher �8>> Ankunft am Spawn in 10 Sekunden");
							p.setGameMode(GameMode.ADVENTURE);
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*10, 10));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20*10, 10));
							p.playSound(p.getLocation(), Sound.ENTITY_MINECART_RIDING, 1, 1);
							
							Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
								
								@Override
								public void run() {
									
									p.sendMessage("�5Zuglautsprecher �8>> Ankunft am Spawn ...");
									p.sendMessage("�5Zuglautsprecher �8>> Beehren sie uns bald wieder!");
									p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
									p.removePotionEffect(PotionEffectType.INVISIBILITY);
									p.setGameMode(GameMode.SURVIVAL);
									p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
									p.teleport(spawnstation);										
								}
							}, 200);
							
							
							
						} else if(objective.equalsIgnoreCase("Wildniss")) {
							
						} else if(objective.equalsIgnoreCase("Villenviertel")) {
							
						} else if(objective.equalsIgnoreCase("Siedlung")) {
							
						}
						
						
						
					}
					
					
				}
				
				
			}
			
			
		}
		
		
	}

}
