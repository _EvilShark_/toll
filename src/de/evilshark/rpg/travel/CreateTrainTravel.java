package de.evilshark.rpg.travel;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import de.evilshark.rpg.common.Main;

public class CreateTrainTravel implements Listener {
	
	@EventHandler
	public void addSign(SignChangeEvent e) {
		
		
		Player p = (Player) e.getPlayer();
		
		if(e.getLine(0).equalsIgnoreCase("[train]")) {
			
			if(!(e.getLine(1).isEmpty())) {
				if(e.getLine(1).equalsIgnoreCase("travel")) {
					
					if(!(e.getLine(2).isEmpty())) {
						
						if(e.getLine(2).equalsIgnoreCase("downtown")) {

							if(p.hasPermission("sharkrpg.admin.signs")) {
								
								e.setLine(0, null);
								e.setLine(1, "�4Bahnhof");
								e.setLine(2, "�2Ziel: Downtown");
								
							}
							
							
						} else if(e.getLine(2).equalsIgnoreCase("spawn")) {
							
							if(p.hasPermission("sharkrpg.admin.signs")) {
								
								e.setLine(0, null);
								e.setLine(1, "�4Bahnhof");
								e.setLine(2, "�2Ziel: Spawn");
								
							}
							
						} else if(e.getLine(2).equalsIgnoreCase("wild")) {
							
							if(p.hasPermission("sharkrpg.admin.signs")) {
								
								e.setLine(0, null);
								e.setLine(1, "�4Bahnhof");
								e.setLine(2, "�2Ziel: Wildniss");
								
							}
							
						} else if(e.getLine(2).equalsIgnoreCase("town")) {
							
							if(p.hasPermission("sharkrpg.admin.signs")) {
								
								e.setLine(0, null);
								e.setLine(1, "�4Bahnhof");
								e.setLine(2, "�2Ziel: Siedlung");
								
							}
							
						} else if(e.getLine(2).equalsIgnoreCase("rich")) {
							
							if(p.hasPermission("sharkrpg.admin.signs")) {
								
								e.setLine(0, null);
								e.setLine(1, "�4Bahnhof");
								e.setLine(2, "�2Ziel: Villenviertel");
								
							}
							
						} else {
							
							if(p.hasPermission("sharkrpg.admin.signs")) {
								p.sendMessage(Main.prefix+"�cDas Ziel wurde nicht gefunden!");
								e.setLine(0, null);
								e.setLine(1, "�4ERROR");
							}
							
							
						}
						
						
					} 
					
					
				} else if(e.getLine(1).equalsIgnoreCase("buy")) {
					
					if(!(e.getLine(2).isEmpty())) {
						
						try {
							
							int price = Integer.parseInt(e.getLine(2));
							e.setLine(0, null);
							e.setLine(1, "�4Bahnhof");
							e.setLine(2, "�5Ticketpreis: "+price+"�5$");
							
						} catch(Exception exe) {
							
							if(p.hasPermission("sharkrpg.admin.signs")) { 
								p.sendMessage(Main.prefix+"�cBitte verwende einen g�ltigen Preis!");
								e.setLine(0, null);
								e.setLine(1, "�4ERROR");
							}
						}
						
						
					} else {
						if(p.hasPermission("sharkrpg.admin.signs")) { 
							p.sendMessage(Main.prefix+"�cBitte verwende einen g�ltigen Preis!");
							e.setLine(0, null);
							e.setLine(1, "�4ERROR");
						}
					}
					
				} else {
					
					if(p.hasPermission("sharkrpg.admin.signs")) {
						p.sendMessage(Main.prefix+"�cDer Modus wurde nicht gefunden!");
						e.setLine(0, null);
						e.setLine(1, "�4ERROR");
					}
				}
			}
			
		}
		
		
		
	}

}
