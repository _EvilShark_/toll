package de.evilshark.rpg.common;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DeathListener implements Listener {
	
	public static File file = new File("plugins/SharkRPG", "playerdata.yml");
	public static FileConfiguration playerdata = YamlConfiguration.loadConfiguration(file);
	public static FileConfiguration cfg = Main.main.getConfig();
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
	
		
		Player victim = e.getEntity();
		Entity killer = victim.getKiller();
		
		double X = victim.getLocation().getX();
		double Y = victim.getLocation().getY();
		double Z = victim.getLocation().getZ();
		
		for(Player online : Bukkit.getOnlinePlayers()) {
		
			if(online.hasPermission("sharkrpg.jobs.police")) {
				
				ItemStack duty = new ItemStack(Material.WOOL);
				ItemMeta meta = duty.getItemMeta();
				
				meta.setDisplayName("ON DUTY");
				meta.addEnchant(Enchantment.ARROW_INFINITE, 10, true);
				duty.setItemMeta(meta);
				
				if(online.getInventory().contains(duty)) {
					
					online.sendMessage("�6Dispatch �8>> Es gab einen �4Todesfall �2CODE 3");
					online.sendMessage("�6Dispatch �8>> Location: X: "+X+" Y: "+Y+" Z: "+Z);
					
				}
				
				
			}
			
		
		
		}
		
		if(killer instanceof Player) {
			
			Player pkiller = victim.getKiller();
			
			Main.loadBank();
			int money = Main.finance.getInt(victim.getName() + ".onHand");
			int old = Main.finance.getInt(pkiller.getName() + ".onHand");
			
			if(!(money < 1)) {
				pkiller.sendMessage(Main.prefix+"Du hast das Geld des Opfers erhalten!");

				Main.loadBank();
				Main.finance.set(pkiller.getName() + ".onHand", old + money);
				Main.saveBank();
				
			}
			
			e.setDeathMessage("�cDer Spieler �r"+victim.getDisplayName()+" �cwurde von �r"+pkiller.getDisplayName()+" �cget�tet!");
			respawnManager(victim);
		} else {
			
			e.setDeathMessage("�cDer Spieler �r"+victim.getDisplayName()+" �cist gestorben!");
			respawnManager(victim);
			
		}
		
		
	}
	
	@SuppressWarnings("deprecation")
	public static void respawnManager(Player p) {
		
		
		String cworld = cfg.getString("Config.Citywelt");
		
		double X = -15.700;
		double Y = 78.000;
		double Z = 49.331;
		
		p.spigot().respawn();
		
		Location gspawn = new Location(Bukkit.getServer().getWorld(cworld), X, Y, Z);
		
		p.teleport(gspawn);
		
		p.sendTitle("�4Du bist gestorben", null);
		p.sendMessage(Main.prefix+"�cDu wachst im Krankenhaus auf!");
		p.sendMessage(Main.prefix+"�cDie Krankenhauskosten betragen 200 $");
		p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 180, 1));
		p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 180, 1));
		Main.loadBank();
		Main.finance.set(p.getName() + ".onHand", 0);
		Main.saveBank();
		
		Main.loadBank();
		int mhand = Main.finance.getInt(p.getName() + ".onHand");
		int mbank = Main.finance.getInt(p.getName() + ".onBank");
		int money = mhand + mbank;

		
		if(money < 200) {
			Main.loadBank();
			Main.finance.set(p.getName() + ".onHand", 0);
			Main.finance.set(p.getName() + ".onBank", 0);
			p.sendMessage(Main.prefix+"Dir werden die Kosten zum Teil erlassen!");
			Main.saveBank();
		} else {
			Main.loadBank();
			Main.finance.set(p.getName() + ".onBank", mbank - 200);
			Main.saveBank();
		}
		
		
		
	}
	

}
