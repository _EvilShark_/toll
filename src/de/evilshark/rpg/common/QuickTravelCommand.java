package de.evilshark.rpg.common;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class QuickTravelCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(cs instanceof Player) {
			
			Player p = (Player) cs;
			
			if(args.length == 1) {
				
				String warpname = args[0].toLowerCase();
				
				if(Main.quicktravel.contains(warpname.toLowerCase())) {
					
					String worldname = Main.quicktravel.getString(warpname + ".world");
					double X = Main.quicktravel.getDouble(warpname + ".X");
					double Y = Main.quicktravel.getDouble(warpname + ".Y");
					double Z = Main.quicktravel.getDouble(warpname + ".Z");
					double Pitch = Main.quicktravel.getDouble(warpname + ".Pitch");
					double Yaw = Main.quicktravel.getDouble(warpname + ".Yaw");
					
					Location warp = new Location(Bukkit.getServer().getWorld(worldname), X, Y, Z);
					p.teleport(warp);
					p.getLocation().setPitch((float) Pitch);
					p.getLocation().setYaw((float) Yaw);
					p.sendMessage(Main.prefix+"Du wurdest teleportiert!");
					
					
				} else {
					
					p.sendMessage(Main.prefix+"�4FEHLER: �8Dieser WARP exestiert nicht!");
					
				}
				
				
				
			} else if(args.length == 2) {
				
				if(args[0].equalsIgnoreCase("set")) {
					
					String warpname = args[1];
					
					String worldname = p.getWorld().getName();
					double X = p.getLocation().getX();
					double Y = p.getLocation().getY();
					double Z = p.getLocation().getZ();
					double Pitch = p.getLocation().getPitch();
					double Yaw = p.getLocation().getYaw();
					
					if(Main.quicktravel.contains(warpname.toLowerCase())) {
						
						p.sendMessage(Main.prefix+"�4FEHLER: �8Dieser WARP Punkt exestiert bereits!");
						
					} else {
						
						Main.quicktravel.set(warpname.toLowerCase() + ".world", worldname);
						Main.quicktravel.set(warpname.toLowerCase() + ".X", X);
						Main.quicktravel.set(warpname.toLowerCase() + ".Y", Y);
						Main.quicktravel.set(warpname.toLowerCase() + ".Z", Z);
						Main.quicktravel.set(warpname.toLowerCase() + ".Pitch", Pitch);
						Main.quicktravel.set(warpname.toLowerCase() + ".Yaw", Yaw);
						Main.saveTravel();
						
						p.sendMessage(Main.prefix+"Der WARP Punkt wurde erstellt!");
						
					}
					
					
				} else if(args[0].equalsIgnoreCase("remove")) {
					
					String warpname = args[1];
					
					if(Main.quicktravel.contains(warpname.toLowerCase())) {
						
						
						p.sendMessage(Main.prefix+"Der WARP Punkt wurde gel�scht!");
						
						Main.quicktravel.set(warpname.toLowerCase(), null);
						Main.saveTravel();
					
					
					} else {
					
						p.sendMessage(Main.prefix+"�4FEHLER: �8Dieser WARP Punkt exestiert nicht!");
						
					}
					
					
				} else {
					
					p.sendMessage(Main.argument);
					
				}
				
				
			} else {
				
				cs.sendMessage(Main.argument);
				
			}
			
			
			
		} else {
			
			cs.sendMessage(Main.console);
			
		}	
		

		return true;
	}
	
	

}
