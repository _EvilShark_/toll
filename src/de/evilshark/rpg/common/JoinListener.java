package de.evilshark.rpg.common;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		
		Player p = e.getPlayer();
		
		String pname = e.getPlayer().getName();
		
		e.setJoinMessage("�6Server �8>> Der Spieler"+""+p.getDisplayName()+" �8hat den Server �2betreten�8!");
		
		FileConfiguration cfg = Main.main.getConfig();
		
		String cworld = cfg.getString("Config.Citywelt");
		double X = cfg.getDouble("Citywelt.X");
		double Y = cfg.getDouble("Citywelt.Y");
		double Z = cfg.getDouble("Citywelt.Z");
		double Pitch = cfg.getDouble("Citywelt.Pitch");
		double Yaw = cfg.getDouble("Citywelt.Yaw");
		
		Location gspawn = new Location(Bukkit.getServer().getWorld(cworld), X, Y, Z);
		
		p.teleport(gspawn);
		p.getLocation().setPitch((float) Pitch);
		p.getLocation().setYaw((float) Yaw);;
		
		Main.loadStats();
		
		if(!(Main.playerdata.contains(pname))) {
			
			Bukkit.broadcastMessage("�6Server �8>> �2Der Spieler �r"+p.getDisplayName()+" �2besucht uns zum ersten Mal!");
			p.sendMessage(Main.prefix+"Willkommen auf unserem Server!");
			p.sendMessage(Main.prefix+"Tippe /tutorial ein, wenn du Hilfe ben�tigst!");
			p.sendTitle("�1"+p.getName(), "�6Viel Spa�!");
			
			
			Main.playerdata.set(pname + ".job", "null");
			Main.playerdata.set(pname + ".jail", 0);
			Main.playerdata.set(pname + ".education", 0);
			Main.playerdata.set(pname + ".sports", 0);
			Main.playerdata.set(pname + ".title", 0);
			Main.playerdata.set(pname + ".allowShop", false);
			Main.playerdata.set(pname + ".drugDealing", false);
			Main.playerdata.set(pname + ".allowSchool", true);
			Main.playerdata.set(pname + ".inHotel", false);

			Main.saveStats();
			
			Main.drugs.set(pname + ".koks", 0);
			Main.drugs.set(pname + ".meth", 0);
			Main.drugs.set(pname + ".grass", 0);
			Main.drugs.set(pname + ".heroin", 0);
			
			Main.saveDrugs();
			
			Main.finance.set(pname + ".onHand", 0);
			Main.finance.set(pname + ".onBank", 500);
			Main.finance.set(pname + ".tax", 0);
			Main.finance.set(pname + ".taxTime", 0);
			
			Main.saveBank();
			
			
			
		}
		
	}

}
