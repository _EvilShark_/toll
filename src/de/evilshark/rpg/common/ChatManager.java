package de.evilshark.rpg.common;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatManager implements Listener {
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		
		Player p = e.getPlayer();
		String msg = e.getMessage();
		
		File file = new File("plugins/SharkRPG", "playerdata.yml");
		FileConfiguration playerdata = YamlConfiguration.loadConfiguration(file);
		FileConfiguration cfg = Main.main.getConfig();
		
		String farm = cfg.getString("Config.Farmwelt");
		String city = cfg.getString("Config.Citywelt");
		String pvp = cfg.getString("Config.PVPWelt");
		String pworld = p.getWorld().getName();
		
		int title = playerdata.getInt(p.getName()+".title");
		
		if(pworld.equals(city)) {
			if(title == 0) {
				e.setFormat("�8[CITY] �d"+p.getName()+" �8>> "+msg);
			} else if(title == 1) {
				e.setFormat("�8[CITY] �1Officer �d"+p.getName()+" �8>> "+msg);
			} else if(title == 2) {
				e.setFormat("�8[CITY] �2Dr. �d"+p.getName()+" �8>> "+msg);
			} else if(title == 3) {
				e.setFormat("�8[CITY] �cProf. �d"+p.getName()+" �8>> "+msg);
			} else if(title == 4) {
				e.setFormat("�8[CITY] �5Beamter �d"+p.getName()+" �8>> "+msg);
			} else if(title == 5) {
				e.setFormat("�8[CITY] �6Kanzler �d"+p.getName()+" �8>> "+msg);
			} else if(title == 6) {
				e.setFormat("�8[CITY] �4ADMIN "+p.getName()+" �8>> "+msg);
			}
		} else if(pworld.equals(farm)) {
			if(title == 0) {
				e.setFormat("�8[FARMWELT] �d"+p.getName()+" �8>> "+msg);
			} else if(title == 1) {
				e.setFormat("�8[FARMWELT] �1Officer �d"+p.getName()+" �8>> "+msg);
			} else if(title == 2) {
				e.setFormat("�8[FARMWELT] �2Dr. �d"+p.getName()+" �8>> "+msg);
			} else if(title == 3) {
				e.setFormat("�8[FARMWELT] �cProf. �d"+p.getName()+" �8>> "+msg);
			} else if(title == 4) {
				e.setFormat("�8[FARMWELT] �5Beamter �d"+p.getName()+" �8>> "+msg);
			} else if(title == 5) {
				e.setFormat("�8[FARMWELT] �6Kanzler �d"+p.getName()+" �8>> "+msg);
			} else if(title == 6) {
				e.setFormat("�8[FARMWELT] �4ADMIN "+p.getName()+" �8>> "+msg);
			}
		} else if(pworld.equals(pvp)) {
			if(title == 0) {
				e.setFormat("�8[PVP] �d"+p.getName()+" �8>> "+msg);
			} else if(title == 1) {
				e.setFormat("�8[PVP] �1Officer �d"+p.getName()+" �8>> "+msg);
			} else if(title == 2) {
				e.setFormat("�8[PVP] �2Dr. �d"+p.getName()+" �8>> "+msg);
			} else if(title == 3) {
				e.setFormat("�8[PVP] �cProf. �d"+p.getName()+" �8>> "+msg);
			} else if(title == 4) {
				e.setFormat("�8[PVP] �5Beamter �d"+p.getName()+" �8>> "+msg);
			} else if(title == 5) {
				e.setFormat("�8[PVP] �6Kanzler �d"+p.getName()+" �8>> "+msg);
			} else if(title == 6) {
				e.setFormat("�8[PVP] �4ADMIN "+p.getName()+" �8>> "+msg);
			}
		}
		
	}

}
