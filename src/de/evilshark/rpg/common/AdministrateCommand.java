package de.evilshark.rpg.common;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.rpg.hotel.AdministrateHotel;
import de.evilshark.rpg.police.SetPoliceOfficer;

public class AdministrateCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkrpg.admin")) {
			
			if(cs instanceof Player) {
				
				Player p = (Player) cs;
				
				if(args.length == 3) {
					
					if(args[0].equalsIgnoreCase("hotel")) {
						
						if(args[1].equalsIgnoreCase("setroom")) {
							
							try  {
								
								@SuppressWarnings("unused")
								int room = Integer.parseInt(args[2]);
								
								if(Main.hotel.contains(args[2])) {
									
									AdministrateHotel.addRoom(p, args[2]);
									
								} else {
									
									p.sendMessage("�4ADMINISTRATION �8>> �4FEHLER: Diese Zahl ist ung�ltig!");
									
								}
								
							} catch(Exception exe) {
								
								p.sendMessage("�4FEHLER: Diese Zahl ist ung�ltig!");
								
							}
							
							
						}
						
						
					}
					
					
				} else if(args.length == 2) {
					
					if(args[0].equalsIgnoreCase("police")) {
						
						if(args[1].equalsIgnoreCase("setchief")) {
						
							SetPoliceOfficer.addNpc(p);
							
							
						} else {
							
							p.sendMessage(Main.argument);;
							
						}
						
						
						
					} else {
						
						p.sendMessage(Main.argument);;
						
					}
					
					
					
				} else {
					
					p.sendMessage(Main.argument);;
					
				}
				
				
			} else {
				
				cs.sendMessage(Main.console);
				
			}
			
			
			 
		} else {
			
			cs.sendMessage(Main.permission);
			
		}
		
		
		
		
		return true;
	}
	

}
