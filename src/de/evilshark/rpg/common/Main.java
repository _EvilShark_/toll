package de.evilshark.rpg.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.evilshark.rpg.crime.DrugDealing;
import de.evilshark.rpg.crime.DrugShop;
import de.evilshark.rpg.finance.MoneyCommand;
import de.evilshark.rpg.hotel.CheckIn;
import de.evilshark.rpg.hotel.EnterRoom;
import de.evilshark.rpg.hotel.HotelCommand;
import de.evilshark.rpg.life.CellPhone;
import de.evilshark.rpg.police.EmergencyCommand;
import de.evilshark.rpg.police.PoliceDuty;
import de.evilshark.rpg.police.RemoveChief;
import de.evilshark.rpg.travel.CreateTrainTravel;
import de.evilshark.rpg.travel.TrainTravel;

public class Main extends JavaPlugin {
	
	public static Main main;
	
	public static Main getInstance() {
		return main;
	}
	
	public static String prefix = "�1CityBuild �8>> ";
	public static String argument = prefix+"�4FEHLER�8: �cBitte �berpr�fe die Argumente!";
	public static String console = prefix+"�4FEHLER�8: �cDieser Command kann nur von der Konsole benutzt werden!";
	public static String offline = prefix+"�4FEHLER�8: �cDieser Spieler wurde nicht gefunden!";
	public static String permission = prefix+"�4FEHLER�8: �cDu hast nicht gen�gend Rechte!";
	
	public static File file = new File("plugins/SharkRPG", "playerdata.yml");
	public static FileConfiguration playerdata = YamlConfiguration.loadConfiguration(file);
	
	public static File file2 = new File("plugins/SharkRPG", "drug.yml");
	public static FileConfiguration drugs = YamlConfiguration.loadConfiguration(file2);
	
	public static File file3 = new File("plugins/SharkRPG", "finance.yml");
	public static FileConfiguration finance = YamlConfiguration.loadConfiguration(file3);
	
	public static File file4 = new File("plugins/SharkRPG", "quicktravel.yml");
	public static FileConfiguration quicktravel = YamlConfiguration.loadConfiguration(file4);
	
	public static File file5 = new File("plugins/SharkRPG", "jobs.yml");
	public static FileConfiguration jobs = YamlConfiguration.loadConfiguration(file5);
	
	public static File file6 = new File("plugins/SharkRPG", "hotel.yml");
	public static FileConfiguration hotel = YamlConfiguration.loadConfiguration(file6);
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static HashMap<Player, Player> dial = new HashMap();
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static HashMap<Player, Player> talk = new HashMap();
	@SuppressWarnings("rawtypes")
	public static final ArrayList emergency = new ArrayList();
	@SuppressWarnings("rawtypes")
	public static final ArrayList hotelguest = new ArrayList();
	
	@Override
	public void onEnable() {
		main = this;
		loadConfig();
		
		if((!file.exists())) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!(file2.exists())) {
			try {
				file2.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!(file3.exists())) {
			try {
				file3.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!(file4.exists())) {
			
			try {
				file4.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		if(!(file5.exists())) {
			
			try {
				file5.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		if(!(file6.exists())) {
			
			try {
				file6.createNewFile();
				createHotel();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new JoinListener(), this);
		pm.registerEvents(new LeaveListener(), this);
		pm.registerEvents(new ChatManager(), this);
		pm.registerEvents(new DeathListener(), this);
		pm.registerEvents(new DrugShop(), this);
		pm.registerEvents(new CreateTrainTravel(), this);
		pm.registerEvents(new TrainTravel(), this);
		pm.registerEvents(new CheckIn(), this);
		pm.registerEvents(new EnterRoom(), this);
		pm.registerEvents(new RemoveChief(), this);
		pm.registerEvents(new CellPhone(), this);
		pm.registerEvents(new PoliceDuty(), this);
		
		this.getCommand("drugdealing").setExecutor(new DrugDealing());
		this.getCommand("money").setExecutor(new MoneyCommand());
		this.getCommand("dial").setExecutor(new CellPhone());
		this.getCommand("emergency").setExecutor(new EmergencyCommand());
		this.getCommand("quicktravel").setExecutor(new QuickTravelCommand());
		this.getCommand("hotel").setExecutor(new HotelCommand());
		this.getCommand("administrate").setExecutor(new AdministrateCommand());
		
		System.out.println(prefix+"Das Plugin wurde gestartet!");
		
		for(Player online : Bukkit.getOnlinePlayers()) {
			if(online.hasPermission("sharkrpg.admin.status")) {
				online.sendMessage(prefix+"Das Plugin wurde �2gestartet�8!");
			}
		}
		
		
	
	}
	

	
	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public static void saveStats() {
		try {
			playerdata.save(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadStats() {
		try {
			playerdata.load(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveDrugs() {
		
		try {
			drugs.save(file2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void loadDrugs() {
		
		try {
			drugs.load(file2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void saveBank() {
		
		try {
			finance.save(file3);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void loadBank() {
		
		try {
			finance.load(file3);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void saveTravel() {
		
		try {
			quicktravel.save(file4);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void loadTravel() {
		
		try {
			quicktravel.load(file4);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void saveJobs() {
		
		try {
			jobs.save(file5);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void loadJobs() {
		
		try {
			jobs.load(file5);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void saveHotel() {
		
		try {
			hotel.save(file6);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void loadHotel() {
		
		try {
			hotel.load(file6);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void createHotel() {
		
		hotel.set(101 +".isFree", true);
		hotel.set(102 +".isFree", true);
		hotel.set(103 +".isFree", true);
		hotel.set(104 +".isFree", true);
		hotel.set(105 +".isFree", true);
		hotel.set(106 +".isFree", true);
		
		hotel.set(201 +".isFree", true);
		hotel.set(202 +".isFree", true);
		hotel.set(203 +".isFree", true);
		hotel.set(204 +".isFree", true);
		hotel.set(205 +".isFree", true);
		hotel.set(206 +".isFree", true);
		
		hotel.set(301 +".isFree", true);
		hotel.set(302 +".isFree", true);
		hotel.set(303 +".isFree", true);
		hotel.set(304 +".isFree", true);
		hotel.set(305 +".isFree", true);
		hotel.set(306 +".isFree", true);
		
		hotel.set(401 +".isFree", true);
		hotel.set(402 +".isFree", true);
		hotel.set(403 +".isFree", true);
		hotel.set(404 +".isFree", true);
		hotel.set(405 +".isFree", true);
		hotel.set(406 +".isFree", true);
		
		hotel.set(501 +".isFree", true);
		hotel.set(502 +".isFree", true);
		hotel.set(503 +".isFree", true);
		hotel.set(504 +".isFree", true);
		hotel.set(505 +".isFree", true);
		hotel.set(506 +".isFree", true);
		
		hotel.set(701 +".isFree", true);
		hotel.set(702 +".isFree", true);
		hotel.set(703 +".isFree", true);
		hotel.set(704 +".isFree", true);
		hotel.set(705 +".isFree", true);
		hotel.set(706 +".isFree", true);
		
		hotel.set(801 +".isFree", true);
		hotel.set(802 +".isFree", true);
		hotel.set(803 +".isFree", true);
		hotel.set(804 +".isFree", true);
		hotel.set(805 +".isFree", true);
		hotel.set(806 +".isFree", true);
		
		hotel.set(601 +".isFree", true);
		hotel.set(602 +".isFree", true);
		hotel.set(603 +".isFree", true);
		hotel.set(604 +".isFree", true);
		hotel.set(605 +".isFree", true);
		hotel.set(606 +".isFree", true);
		
		hotel.set(901 +".isFree", true);
		hotel.set(902 +".isFree", true);
		hotel.set(903 +".isFree", true);
		hotel.set(904 +".isFree", true);
		hotel.set(905 +".isFree", true);
		hotel.set(906 +".isFree", true);
		
		saveHotel();
		
		
	}
	
	
}
