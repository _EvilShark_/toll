package de.evilshark.rpg.crime;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.evilshark.rpg.common.Main;

public class DrugDealing implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				Main.loadStats();
				boolean tf = Main.playerdata.getBoolean(p.getName() + ".drugDealing");
				
				if(tf == false ) {
					p.sendMessage(Main.prefix+"�cWarnung: Diese Aktivit�t ist illegal und f�hrt zu Strafen!");
					p.sendMessage(Main.prefix+"Wenn du beginnen m�chtest gib �6/drugdealing confirm �8ein!");
					p.sendMessage(Main.prefix+"Um �cabzubrechen �8gib �6/drugdealing abort �8ein!");
					p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10000000, 200));
				} else {
					p.sendMessage(Main.prefix+"�cDu bist bereits in illegale Gesch�fte verwickelt!");
					p.sendMessage(Main.prefix+"Zum Beenden tippe �6/drugdealing quit �8ein!");
				}
			} else if(args.length == 1) {
				if(args[0].equalsIgnoreCase("confirm")) {
					
					if(p.hasPotionEffect(PotionEffectType.BLINDNESS)) {
						p.sendMessage(Main.prefix+"Du kannst nun Drogen an und verkaufen.");
						p.sendMessage(Main.prefix+"�cBemerkt dich ein Polizist, wirst du verhaftet!");
						p.removePotionEffect(PotionEffectType.BLINDNESS);
						Main.playerdata.set(p.getName() + ".drugDealing", true);
						Main.saveStats();
					} else {
						p.sendMessage(Main.prefix+"�cTippe zuerst �6/drugdealing �cein!");
					}
				} else if(args[0].equalsIgnoreCase("abort")) {
					
					if(p.hasPotionEffect(PotionEffectType.BLINDNESS)) {
						p.sendMessage(Main.prefix+"�cDer Vorgang wurde abgebrochen!");
						p.removePotionEffect(PotionEffectType.BLINDNESS);
					} else {
						p.sendMessage(Main.prefix+"�cTippe zuerst �6/drugdealing �cein!");
					}

				} else if(args[0].equalsIgnoreCase("quit")) {
					Main.loadStats();
					boolean tf = Main.playerdata.getBoolean(p.getName() + ".drugDealing");
					
					if(tf == true) {
						p.sendMessage(Main.prefix+"Du steigst aus den illegalen Gesch�ften aus!");
						Main.loadStats();
						Main.playerdata.set(p.getName() + ".drugDealing", false);
						Main.saveStats();
					} else {
						p.sendMessage(Main.prefix+"�cDu f�hrst derzeit keine illegale Aktivi�t aus!");
					}
					
				} else if(args[0].equalsIgnoreCase("buy")) {
					Main.loadStats();
					boolean tf = Main.playerdata.getBoolean(p.getName() + ".drugDealing");
					
					if(tf == true) {
						buyDrugs(p);
					} else {
						p.sendMessage(Main.prefix+"�cDu bist noch kein Dealer! �8--> �6/drugdealing");
					}
					
				} else if(args[0].equalsIgnoreCase("sell")) {
					Main.loadStats();
					boolean tf = Main.playerdata.getBoolean(p.getName() + ".drugDealing");
					
					if(tf == true) {
						sellDrugs(p);
					} else {
						p.sendMessage(Main.prefix+"�cDu bist noch kein Dealer! �8--> �6/drugdealing");
					}
				}
			}
			
			
		} else {
			cs.sendMessage(Main.console);
		}
		
		
		return true;
	}
	
	
	public static void buyDrugs(Player p) {
		
		double AXspot1 = 169;
		double Yspot1 = 77;
		double AZspot1 = -22;
		double BXspot1 = 167;
		double BZspot1 = -20;
		
		double PX = p.getLocation().getX();
		double PY = p.getLocation().getY();
		double PZ = p.getLocation().getZ();
		
		if(!(PY < Yspot1)) {
			if(p.getWorld().getName().equals("world")) {
				
				if(!(PX > AXspot1)) {
					if(!(PX < BXspot1)) {
						if(!(PZ < AZspot1)) {
							if(!(PZ > BZspot1)) {
								
								Inventory shop = Bukkit.createInventory(null, 9, "�6Buy Drugs");
								
								ItemStack koks = new ItemStack(Material.SUGAR);
								ItemMeta koksm =koks.getItemMeta();
								
								koksm.setDisplayName("�1Kokain: �6400$");
								koks.setItemMeta(koksm);
								
								ItemStack heroin = new ItemStack(Material.GLOWSTONE_DUST);
								ItemMeta heroinm = heroin.getItemMeta();
								
								heroinm.setDisplayName("�5Heroin: �6400$");
								heroin.setItemMeta(heroinm);
								
								ItemStack grass = new ItemStack(Material.GRASS);
								ItemMeta grassm = grass.getItemMeta();
								
								grassm.setDisplayName("�2Mariuahna: �6300$");
								grass.setItemMeta(grassm);
								
								ItemStack meth = new ItemStack(Material.REDSTONE);
								ItemMeta methm = meth.getItemMeta();
								
								methm.setDisplayName("�6Meth: �6500$");
								meth.setItemMeta(methm);
								
								shop.setItem(0, koks);
								shop.setItem(1, heroin);
								shop.setItem(2, grass);
								shop.setItem(3, meth);
								
								p.openInventory(shop);
								
							} else {
								p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
							}
						} else {
							p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
						}
					} else {
						p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
					}
				} else {
					p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
				}
			} else {
				p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
			}
		} else {
			p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
		}
			
	}
	
	public static void sellDrugs(Player p) {
		
		double AXspot1 = 169;
		double Yspot1 = 77;
		double AZspot1 = -22;
		double BXspot1 = 167;
		double BZspot1 = -20;
		
		double PX = p.getLocation().getX();
		double PY = p.getLocation().getY();
		double PZ = p.getLocation().getZ();
		
		if(!(PY < Yspot1)) {
			if(p.getWorld().getName().equals("world")) {
				
				if(!(PX > AXspot1)) {
					if(!(PX < BXspot1)) {
						if(!(PZ < AZspot1)) {
							if(!(PZ > BZspot1)) {
								
								Inventory shop = Bukkit.createInventory(null, 9,"�6Sell Drugs");
								
								ItemStack koks = new ItemStack(Material.SUGAR);
								ItemMeta koksm = koks.getItemMeta();
								
								koksm.setDisplayName("�1Kokain: �6550$");
								koks.setItemMeta(koksm);
								
								ItemStack heroin = new ItemStack(Material.GLOWSTONE_DUST);
								ItemMeta heroinm = heroin.getItemMeta();
								
								heroinm.setDisplayName("�5Heroin: �6500$");
								heroin.setItemMeta(heroinm);
								
								ItemStack grass = new ItemStack(Material.GRASS);
								ItemMeta grassm = grass.getItemMeta();
								
								grassm.setDisplayName("�2Mariuahna: �6350$");
								grass.setItemMeta(grassm);
								
								ItemStack meth = new ItemStack(Material.REDSTONE);
								ItemMeta methm = meth.getItemMeta();
								
								methm.setDisplayName("�6Meth: �6750$");
								meth.setItemMeta(methm);
								
								shop.setItem(0, koks);
								shop.setItem(1, heroin);
								shop.setItem(2, grass);
								shop.setItem(3, meth);
								
								p.openInventory(shop);
								
								
								
								
							} else {
								p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
							}
						} else {
							p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
						}
					} else {
						p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
					}
				} else {
					p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
				}
			} else {
				p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
			}
		} else {
			p.sendMessage(Main.prefix+"�cBegebe dich zuerst zu einem Verkaufsplatz! --> �6/drugdealing list");
		}
		
	}
	
	
	
	
	

}
