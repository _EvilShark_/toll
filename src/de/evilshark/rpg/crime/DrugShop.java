package de.evilshark.rpg.crime;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.evilshark.rpg.common.Main;

public class DrugShop implements Listener {
	
	@EventHandler
	public void onBuy(InventoryClickEvent e) {
		
		Player p = (Player) e.getWhoClicked();
		
		Main.loadBank();
		Main.loadDrugs();
		int money = Main.finance.getInt(p.getName() + ".onHand");
		int koksv = Main.drugs.getInt(p.getName() + ".koks");
		int heroinv = Main.drugs.getInt(p.getName() + ".heroin");
		int grassv = Main.drugs.getInt(p.getName() + ".grass");
		int methv = Main.drugs.getInt(p.getName() + ".meth");
		
		
		Material heroin = Material.GLOWSTONE_DUST;
		Material koks = Material.SUGAR;
		Material grass = Material.GRASS;
		Material meth = Material.REDSTONE;
		
		String iname = e.getInventory().getName();
		String buy = "�6Buy Drugs";
		String sell = "�6Sell Drugs";
		
		if(iname.equals(buy)) {
			
			if(e.getCurrentItem().getType().equals(heroin)) {
				
				if(money < 400) {
					p.sendMessage(Main.prefix+"�cDu ben�tigst mehr Geld!");
					e.setCancelled(true);
				} else {
					Main.loadBank();
					Main.finance.set(p.getName() + ".onHand", money - 400);
					Main.saveBank();
					p.sendMessage(Main.prefix+"Dir wurde 1x Heroin hinzugef�gt!");
					
					Main.loadDrugs();
					int oldd = Main.drugs.getInt(p.getName() + ".heroin");
					Main.drugs.set(p.getName() + ".heroin", oldd + 1);
					Main.saveDrugs();
					e.setCancelled(true);
				}
			} else if(e.getCurrentItem().getType().equals(meth)) {
				
				if(money < 500) {
					p.sendMessage(Main.prefix+"�cDu ben�tigst mehr Geld!");
					e.setCancelled(true);
				} else {
					Main.loadBank();
					Main.finance.set(p.getName() + ".onHand", money - 500);
					Main.saveBank();
					p.sendMessage(Main.prefix+"Dir wurde 1x Meth hinzugef�gt!");
					
					Main.loadDrugs();
					int oldd = Main.drugs.getInt(p.getName() + ".meth");
					Main.drugs.set(p.getName() + ".meth", oldd + 1);
					Main.saveDrugs();
					e.setCancelled(true);
				}
			} else if(e.getCurrentItem().getType().equals(grass)) {
				
				if(money < 300) {
					p.sendMessage(Main.prefix+"�cDu ben�tigst mehr Geld!");
					e.setCancelled(true);
				} else {
					Main.loadBank();
					Main.finance.set(p.getName() + ".onHand", money - 300);
					Main.saveBank();
					p.sendMessage(Main.prefix+"Dir wurde 1x Mariuhana hinzugef�gt!");
					
					Main.loadDrugs();
					int oldd = Main.drugs.getInt(p.getName() + ".grass");
					Main.drugs.set(p.getName() + ".grass", oldd + 1);
					Main.saveDrugs();
					e.setCancelled(true);
				}
			} else if(e.getCurrentItem().getType().equals(koks)) {
				
				if(money < 400) {
					p.sendMessage(Main.prefix+"�cDu ben�tigst mehr Geld!");
					e.setCancelled(true);
				} else {
					Main.loadBank();
					Main.finance.set(p.getName() + ".onHand", money - 400);
					Main.saveBank();
					p.sendMessage(Main.prefix+"Dir wurde 1x Kokain hinzugef�gt!");
					
					Main.loadDrugs();
					int oldd = Main.drugs.getInt(p.getName() + ".koks");
					Main.drugs.set(p.getName() + ".koks", oldd + 1);
					Main.saveDrugs();
					e.setCancelled(true);
				}
			}
		
		} else if(iname.equals(sell)) {
			
			if(e.getCurrentItem().getType().equals(heroin)) {
				
				Main.loadDrugs();
				if(heroinv == 0) {
					p.sendMessage(Main.prefix+"�cDu hast keine Drogen dieses Typs!");
					e.setCancelled(true);
				} else {
				
				Main.loadBank();
				Main.finance.set(p.getName() + ".onHand", money + 500);
				Main.saveBank();
				
				Main.loadDrugs();
				Main.drugs.set(p.getName() + ".heroin", heroinv - 1);
				Main.saveDrugs();
				
				p.sendMessage(Main.prefix+"Du hast die Drogen �2erfolgreich verkauft!");
				e.setCancelled(true);
					}
			} else if(e.getCurrentItem().getType().equals(meth)) {
				
				Main.loadDrugs();
				if(methv == 0) {
					p.sendMessage(Main.prefix+"�cDu hast keine Drogen dieses Typs!");
					e.setCancelled(true);
				} else {
				
				Main.loadBank();
				Main.finance.set(p.getName() + ".onHand", money + 750);
				Main.saveBank();
				
				Main.loadDrugs();
				Main.drugs.set(p.getName() + ".meth", methv - 1);
				Main.saveDrugs();
				
				p.sendMessage(Main.prefix+"Du hast die Drogen �2erfolgreich verkauft!");
				e.setCancelled(true);
					}
			} else if(e.getCurrentItem().getType().equals(grass)) {
				
				Main.loadDrugs();
				if(grassv == 0) {
					p.sendMessage(Main.prefix+"�cDu hast keine Drogen dieses Typs!");
					e.setCancelled(true);
				} else {
				
				Main.loadBank();
				Main.finance.set(p.getName() + ".onHand", money + 350);
				Main.saveBank();
				
				Main.loadDrugs();
				Main.drugs.set(p.getName() + ".grass", grassv - 1);
				Main.saveDrugs();
				
				p.sendMessage(Main.prefix+"Du hast die Drogen �2erfolgreich verkauft!");
				e.setCancelled(true);
					}
				
			} else if(e.getCurrentItem().getType().equals(koks)) {
				
				Main.loadDrugs();
				if(koksv == 0) {
					p.sendMessage(Main.prefix+"�cDu hast keine Drogen dieses Typs!");
					e.setCancelled(true);
				} else {
				
				Main.loadBank();	
				Main.finance.set(p.getName() + ".onHand", money + 550);
				Main.saveBank();
				
				Main.loadDrugs();
				Main.drugs.set(p.getName() + ".koks", koksv - 1);
				Main.saveDrugs();
				
				p.sendMessage(Main.prefix+"Du hast die Drogen �2erfolgreich verkauft!");
				e.setCancelled(true);
					}
				
			}
		}
		
		
	}

}
