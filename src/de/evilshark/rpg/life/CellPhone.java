package de.evilshark.rpg.life;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.evilshark.rpg.common.Main;
import de.evilshark.rpg.police.Hotline;

public class CellPhone implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				p.sendMessage("�12&2 �8>> Syntax: /dial [Nummer] / [book] / [player]");
			
			} else if(args.length == 1) {
				
				if(args[0].equalsIgnoreCase("911")) {
					Hotline.policePhone(p);
				} else if(args[0].equalsIgnoreCase("08001564")) {
					
				} else if(args[0].equalsIgnoreCase("book")) {
					
					p.sendMessage("�8============�5TELEFONBUCH�8============");
					p.sendMessage("�8Polizei: �6911");
					p.sendMessage("�8Support: �608001564");
					
				} else {
					
					Player p2 = Bukkit.getPlayer(args[0]);
					
					if(p2 != null && p2.isOnline() ) {
						
						if(Main.dial.containsKey(p) || Main.dial.containsValue(p) || Main.talk.containsKey(p) || Main.talk.containsValue(p)) {
							
							p.sendMessage("�12&2 �8>> �cDer Anschluss ist besetzt!");
							
							
						} else {
							
							
							Main.loadBank();
							int money = Main.finance.getInt(p.getName() + ".onBank");
							
							if(money < 10) {
								p.sendMessage("�12&2 �8>> �cSie haben nicht genug Geld auf dem Konto!");
							} else {
								Main.loadBank();
								Main.finance.set(p.getName() + ".onBank", money - 10);
								Main.saveBank();
								
								p.sendMessage("�12&2 �8>> Sie rufen den Spieler �5"+p2.getName()+" �8an!");
								
								Main.dial.put(p2, p);
								p2.sendMessage("21myPhone �8>> Der Spieler �5"+p.getName()+" �8ruft dich an!");
								p2.playSound(p2.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
								p2.sendMessage("�2myPhone �8>> �2/phone accept �4/phone deny");
								
								
								
							}
							
						}
						
						
					} else {
						p.sendMessage("�12&2 �8>> �cDiese Rufnummer ist nicht vergeben!");
					}
					
					
					
					
					
				}
				
			}
			
			
		} else {
			cs.sendMessage(Main.console);
		}
		
		
		return true;
	}
	
	@EventHandler
	public void talk(AsyncPlayerChatEvent e) {
		
		Player p = e.getPlayer();
		
		if(Main.talk.containsKey(p) || Main.talk.containsValue(p)) {
			
			Player p2 = Main.talk.get(p);
			
			String msg = e.getMessage();
			
			p2.sendMessage("�2myPhone �8>> �5*Gespr�ch* �6"+msg);
			p.sendMessage("�2myPhone �8>> �5*Gespr�ch* �6"+msg);
			
			e.setCancelled(true);
			
			
		}
		
		
		
		
		
	}
	
	

}
