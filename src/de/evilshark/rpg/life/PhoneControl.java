package de.evilshark.rpg.life;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.rpg.common.Main;

public class PhoneControl implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(cs instanceof Player) {
			
			Player p = (Player) cs;
			
			if(args.length == 1) {
				
				if(args[0].equalsIgnoreCase("accept")) {
					
					if(Main.dial.containsKey(p)) {
						
						Player p2 = Main.dial.get(p);
						
						p2.sendMessage("�2myPhone �8>> Der Spieler �5"+p.getName()+" �8hat deinen Anruf �2angenommen�8!");
						p.sendMessage("�2myPhone �8>> Du hast den Anruf angenommen!");
						
						Main.talk.put(p, p2);
						Main.dial.remove(p);
						Main.dial.remove(p2);
						
					} else {
						
						p.sendMessage("�2myPhone �8>> �cDu wurdest nicht angerufen!");
						
					}
					
					
					
				} else if(args[0].equalsIgnoreCase("deny")) {
					
					if(Main.dial.containsKey(p)) {
						
						Player p2 = Main.dial.get(p);
						
						p2.sendMessage("�2myPhone �8>> Der Nutzer ist zur Zeit nicht erreichbar!");
						p.sendMessage("myPhone �8>> Du hast den Anruf �cabgelehnt�8!");
						
						Main.dial.remove(p);
						
						
					} else {
						
						p.sendMessage("�2myPhone �8>> �cDu wurdest nicht angerufen!");
						
					}
					
					
					
				} else if(args[0].equalsIgnoreCase("cancel")) {
					
					if(Main.talk.containsKey(p) || Main.talk.containsValue(p)) {
						
						
						Main.talk.remove(p);
						p.sendMessage("�2myPhone �8>> Sie haben das Gespr�ch beendet!");
						
						Player p2 = Main.talk.get(p);
						
						p2.sendMessage("�2myPhone �8>> Das Gespr�ch wurde beendet!");
						
						
					} else {
						
						p.sendMessage("�2myPhone �8>> �cDu f�hrst momentan kein Gespr�ch!");
						
					}
					
					
					
				} else {
					
					p.sendMessage(Main.argument);
					p.sendMessage("�6/phone [accept/deny/cancel]");
					
				}
				
				
				
			} else {
				
				p.sendMessage(Main.argument);
				p.sendMessage("�6/phone [accept/deny/cancel]");
				
			}
			
			
		} else {
			
			cs.sendMessage(Main.console);
			
		}
		

		return true;
	}
	
	

}
